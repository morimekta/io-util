IO Utilities
============

Utilities for facilitating heavy computational IO, specifically reading and
writing typed data to IO streams. The library has grown over time and has
in later time become more of a `commons-lang` type library, which includes
a number of non-I/O but still pretty core language feature classes.

See [docs/index.md](docs/index.md) for details.