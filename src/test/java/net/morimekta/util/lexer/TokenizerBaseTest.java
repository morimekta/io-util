package net.morimekta.util.lexer;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.util.lexer.impl.TestToken;
import net.morimekta.util.lexer.impl.TestTokenType;
import net.morimekta.util.lexer.impl.TestTokenizer;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.function.Function;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class TokenizerBaseTest {
    @DataProvider
    public static Object[][] goodTokens() {
        return new Object[][] {
                // nothing
                {"", listOf()},
                {"# just a comment", listOf()},
                // comments, special newlines
                {"#ignore \r\n" +
                 "{\n" +
                 "  # ignore\n" +
                 "  boo\r\n" +
                 "}\r",
                 listOf("{", "boo", "}")},
                // strings
                {"\"This is a long string, that should be longer than the streaming buffer size.\"",
                 listOf("\"This is a long string, that should be longer than the streaming buffer size.\"")},
                // numbers
                {"0,0", listOf("0", ",", "0")},
                {"12", listOf("12")},
                {"012:", listOf("012", ":")},
                {"0x44,", listOf("0x44", ",")},
                {"0.1235;", listOf("0.1235", ";")},
                {".2345", listOf(".2345")},
                {".2345e-5", listOf(".2345e-5")},
                {"55.2345e+5", listOf("55.2345e+5")},
                // literals
                {"\"foo\"", listOf("\"foo\"")},
                // identifiers
                {"und", listOf("und")},
                {"und t", listOf("und", "t")},
                {"und,t", listOf("und", ",", "t")},
                {"@undefined$other", listOf("@", "undefined", "$", "other")},
                {"$undefined@other", listOf("$", "undefined", "@", "other")},
                // symbols
                {"{[]}&", listOf("{", "[", "]", "}", "&")},
                };
    }

    @Test
    @UseDataProvider("goodTokens")
    public void testGoodTokens_streaming(String content, List<String> expected) throws IOException {
        TestTokenizer tokenizer = streaming(content);

        for (String expect : expected) {
            assertThat(tokenString(tokenizer.parseNextToken()), is(expect));
        }
        assertThat(tokenizer.parseNextToken(), is(nullValue()));
    }

    @Test
    @UseDataProvider("goodTokens")
    public void testGoodTokens_preLoaded(String content, List<String> expected) throws IOException {
        TestTokenizer tokenizer = preLoaded(content);

        for (String expect : expected) {
            assertThat(tokenString(tokenizer.parseNextToken()), is(expect));
        }
        assertThat(tokenizer.parseNextToken(), is(nullValue()));
    }

    @DataProvider
    public static Object[][] dataFailures() {
        return new Object[][] {
                // --- strings
                {"\n\n\"\\\"",
                 listOf(),
                 "Error on line 3 row 4: Unexpected end of stream in string\n" +
                 "\"\\\"\n" +
                 "---^"},
                {"\n\n  \"\n\"",
                 listOf(),
                 "Error on line 3 row 4: Unexpected newline in string\n" +
                 "  \"\n" +
                 "---^"},
                {"\n\n  \"\003\"",
                 listOf(),
                 "Error on line 3 row 4: Unescaped non-printable char in string: '\\003'\n" +
                 "  \"·\"\n" +
                 "---^"},
                {"\n\n  \"\t\"",
                 listOf(),
                 "Error on line 3 row 4: Unescaped non-printable char in string: '\\t'\n" +
                 "  \"·\"\n" +
                 "---^"},
                {"\n\n  \"\u0081\"",
                 listOf(),
                 "Error on line 3 row 4: Unescaped non-printable char in string: '\\u0081'\n" +
                 "  \"·\"\n" +
                 "---^"},

                // --- numbers
                {"5r",
                 listOf(),
                 "Error on line 1 row 1-2: Invalid termination of number: '5r'\n" +
                 "5r\n" +
                 "^^"},
                {"{\n" +
                 "  foo, 5b7\n" +
                 "}\n",
                 listOf("{", "foo", ","),
                 "Error on line 2 row 8-9: Invalid termination of number: '5b'\n" +
                 "  foo, 5b7\n" +
                 "-------^^"},
                {"12f",
                 listOf(),
                 "Error on line 1 row 1-3: Invalid termination of number: '12f'\n" +
                 "12f\n" +
                 "^^^"},
                {"      12f",
                 listOf(),
                 "Error on line 1 row 7-9: Invalid termination of number: '12f'\n" +
                 "      12f\n" +
                 "------^^^"},
                {"-:",
                 listOf(),
                 "Error on line 1 row 1: No decimal after negative indicator\n" +
                 "-:\n" +
                 "^"},
                {"-",
                 listOf(),
                 "Error on line 1 row 1: Negative indicator without number\n" +
                 "-\n" +
                 "^"},
                {".5e:",
                 listOf(),
                 "Error on line 1 row 1-4: Badly terminated number exponent: '.5e:'\n" +
                 ".5e:\n" +
                 "^^^^"},
                {"\n  .5e",
                 listOf(),
                 "Error on line 2 row 3-5: Badly terminated number exponent: '.5e'\n" +
                 "  .5e\n" +
                 "--^^^"},
                {"\n  .5ef",
                 listOf(),
                 "Error on line 2 row 3-6: Badly terminated number exponent: '.5ef'\n" +
                 "  .5ef\n" +
                 "--^^^^"},
                {"\n  .5e2f",
                 listOf(),
                 "Error on line 2 row 3-7: Invalid termination of number: '.5e2f'\n" +
                 "  .5e2f\n" +
                 "--^^^^^"},
                {"\n  .5e+f",
                 listOf(),
                 "Error on line 2 row 3-7: Badly terminated number exponent: '.5e+f'\n" +
                 "  .5e+f\n" +
                 "--^^^^^"},
                {"\n  .5e-",
                 listOf(),
                 "Error on line 2 row 3-6: Badly terminated number exponent: '.5e-'\n" +
                 "  .5e-\n" +
                 "--^^^^"},
                {"\n  4..",
                 listOf(),
                 "Error on line 2 row 3-5: Invalid termination of number: '4..'\n" +
                 "  4..\n" +
                 "--^^^"},
                {"\n  0x",
                 listOf(),
                 "Error on line 2 row 3-4: No decimal after hex indicator\n" +
                 "  0x\n" +
                 "--^^"},
                {"\n  0x3g",
                 listOf(),
                 "Error on line 2 row 3-6: Invalid termination of number: '0x3g'\n" +
                 "  0x3g\n" +
                 "--^^^^"},

                // --- identifiers
                {"e..b",
                 listOf(),
                 "Error on line 1 row 1-3: Identifier with double '.'\n" +
                 "e..b\n" +
                 "^^^"},
                {"e.:",
                 listOf(),
                 "Error on line 1 row 1-2: Identifier with trailing '.'\n" +
                 "e.:\n" +
                 "^^"},
                {"e.7:",
                 listOf(),
                 "Error on line 1 row 1-3: Identifier part with invalid start '7'\n" +
                 "e.7:\n" +
                 "^^^"},

                // bad content
                {"\n\n  \003b",
                 listOf(),
                 "Error on line 3 row 3: Unknown token initiator '\\003'\n" +
                 "  ·b\n" +
                 "--^"},
                };
    }

    @Test
    @UseDataProvider("dataFailures")
    public void testFailures_preLoaded(String content,
                                       List<String> expected,
                                       String displayString) throws IOException {
        TestTokenizer tokenizer = preLoaded(content);

        for (String expect : expected) {
            assertThat(tokenString(tokenizer.parseNextToken()), is(expect));
        }

        try {
            Token token = tokenizer.parseNextToken();
            fail("no exception: " + token);
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        }
    }

    @Test
    @UseDataProvider("dataFailures")
    public void testFailures_streaming(String content,
                                       List<String> expected,
                                       String displayString) throws IOException {
        TestTokenizer tokenizer = streaming(content);

        for (String expect : expected) {
            assertThat(tokenString(tokenizer.parseNextToken()), is(expect));
        }

        try {
            tokenizer.parseNextToken();
            fail("no exception: " + displayString);
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(displayString));
            } catch (AssertionError a) {
                e.printStackTrace();
                a.initCause(e);
                throw a;
            }
        }
    }

    @DataProvider(format = "%m[%i: %p[0..-2]]")
    public static Object[][] tokenizer() {
        return new Object[][] {
                { "preLoaded", (Function<String, TestTokenizer>) TokenizerBaseTest::preLoaded },
                { "streaming", (Function<String, TestTokenizer>) TokenizerBaseTest::streaming },
                };
    }

    @Test
    @UseDataProvider("tokenizer")
    public void testReadUntil(String type, Function<String, TestTokenizer> make) throws IOException {
        TestTokenizer tokenizer;

        assertThat(make.apply(type).readUntil(")", TestTokenType.GENERIC, true).toString(), is(type));
        assertThat(make.apply(type).readUntil("))", TestTokenType.GENERIC, true).toString(), is(type));
        assertThat(make.apply(")").readUntil(")", TestTokenType.GENERIC, true), is(nullValue()));
        assertThat(make.apply("))").readUntil("))", TestTokenType.GENERIC, true), is(nullValue()));

        tokenizer = make.apply("test)");
        assertThat(tokenizer.readUntil(")", TestTokenType.GENERIC, false).toString(), is("test"));
        assertThat(tokenString(tokenizer.parseNextToken()), is(nullValue()));

        tokenizer = make.apply("test)test))");
        assertThat(tokenizer.readUntil("))", TestTokenType.GENERIC, false).toString(), is("test)test"));
        assertThat(tokenString(tokenizer.parseNextToken()), is(nullValue()));

        try {
            tokenizer = make.apply("test");
            tokenizer.readUntil(")", TestTokenType.GENERIC, false);
            fail("no exception");
        } catch (LexerException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 5: End of file while reading until ')'\n" +
                          "test\n" +
                          "----^"));
        }

        try {
            tokenizer = make.apply("test)bar");
            tokenizer.readUntil("))", TestTokenType.GENERIC, false);
            fail("no exception");
        } catch (LexerException e) {
            assertThat(e.displayString(),
                       is("Error on line 1 row 9: End of file while reading until '))'\n" +
                          "test)bar\n" +
                          "--------^"));
        }
    }

    // ----------------------------
    // ----   HELPER METHODS   ----
    // ----------------------------

    private String tokenString(TestToken token) {
        if (token == null) return null;
        return token.toString();
    }

    private static TestTokenizer streaming(String content) {
        StringReader reader = new StringReader(content);
        return new TestTokenizer(reader, 48, false);
    }

    private static TestTokenizer preLoaded(String content) {
        StringReader reader = new StringReader(content);
        return new TestTokenizer(reader, TokenizerBase.DEFAULT_BUFFER_SIZE, true);
    }
}
