package net.morimekta.util.lexer;

import net.morimekta.util.lexer.impl.TestToken;
import net.morimekta.util.lexer.impl.TestTokenType;
import org.junit.Test;

import java.io.IOException;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class TokenizerRepeaterTest {
    @Test
    public void testNextToken() throws IOException {
        TokenizerRepeater<TestTokenType, TestToken> repeater = new TokenizerRepeater<>(
                listOf(token("foo", TestTokenType.IDENTIFIER),
                       token("bar", TestTokenType.GENERIC)));
        assertThat(repeater.toString(), is("TokenizerRepeater{'foo', 'bar'}"));
        assertThat(repeater.currentLine().toString(), is("foo"));
        assertThat(repeater.currentLineNo(), is(1));
        assertThat(repeater.currentLinePos(), is(1));

        assertThat(repeater.parseNextToken(),
                   is(token("foo", TestTokenType.IDENTIFIER)));
        assertThat(repeater.toString(), is("TokenizerRepeater{'bar'}"));

        assertThat(repeater.readUntil(")", TestTokenType.GENERIC, true),
                   is(token("bar", TestTokenType.GENERIC)));
        assertThat(repeater.currentLine().toString(), is("bar"));
        assertThat(repeater.currentLineNo(), is(1));
        assertThat(repeater.currentLinePos(), is(4));
        assertThat(repeater.toString(), is("TokenizerRepeater{}"));

        assertThat(repeater.parseNextToken(), is(nullValue()));
        assertThat(repeater.currentLine().toString(), is("bar"));
        assertThat(repeater.currentLineNo(), is(1));
        assertThat(repeater.currentLinePos(), is(4));
    }

    @Test
    public void testReadUntil() throws IOException {
        TokenizerRepeater<TestTokenType, TestToken> repeater = new TokenizerRepeater<>(
                listOf(token("foo", TestTokenType.IDENTIFIER),
                       token("bar", TestTokenType.GENERIC)));

    }

    private TestToken token(String str, TestTokenType type) {
        return new TestToken(str.toCharArray(), 0, str.length(), type, 1, 1);
    }


}
