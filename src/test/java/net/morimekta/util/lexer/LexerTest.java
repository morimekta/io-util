package net.morimekta.util.lexer;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.lexer.impl.TestLexer;
import net.morimekta.util.lexer.impl.TestToken;
import net.morimekta.util.lexer.impl.TestTokenType;
import net.morimekta.util.lexer.impl.TestTokenizer;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class LexerTest {
    @DataProvider
    public static Object[][] dataDeclaredFailure() {
        return new Object[][] {
                {"foo bar",
                 listOf("foo"),
                 "bar",
                 "Error on line 1 row 5-7: bar\n" +
                 "foo bar\n" +
                 "----^^^"},
                {"foo \n" +
                 "bar \r\n" +
                 "baz",
                 listOf("foo", "bar"),
                 "baz",
                 "Error on line 3 row 1-3: baz\n" +
                 "baz\n" +
                 "^^^"},
        };
    }

    @Test
    @UseDataProvider("dataDeclaredFailure")
    public void testDeclaredFailure_streaming(String content, List<String> expected, String failed, String message)
            throws IOException {
        TestLexer lexer = streaming(content);
        for (String expect : expected) {
            lexer.expect(expect, t -> t.toString().equals(expect));
        }
        TestToken      token     = lexer.expect(failed);
        LexerException exception = lexer.failure(token, failed);
        assertThat(exception, is(instanceOf(LexerException.class)));
        assertThat(exception.displayString(), is(message));
        assertThat(exception.toString(), endsWith(message));
        assertThat(new LexerException(exception, exception.getMessage()).displayString(), is(message));
        assertThat(new LexerException(exception.getMessage()).getMessage(), is(exception.getMessage()));
        assertThat(new LexerException(requireNonNull(exception.getLine()),
                                      exception.getLineNo(),
                                      exception.getLinePos(),
                                      exception.getLength(),
                                      exception.getMessage())
                           .initCause(exception)
                           .displayString(), is(message));
    }

    @Test
    @UseDataProvider("dataDeclaredFailure")
    public void testDeclaredFailure_preLoaded(String content, List<String> expected, String failed, String message)
            throws IOException {
        TestLexer lexer = preLoaded(content);
        for (String expect : expected) {
            lexer.expect(expect, t -> t.toString().equals(expect));
        }
        TestToken      token     = lexer.expect(failed);
        LexerException exception = lexer.failure(token, failed);
        assertThat(exception, is(instanceOf(LexerException.class)));
        assertThat(exception.displayString(), is(message));
        assertThat(exception.toString(), endsWith(message));
        assertThat(new LexerException(exception, exception.getMessage()).displayString(), is(message));
        assertThat(new LexerException(exception.getMessage()).getMessage(), is(exception.getMessage()));
        assertThat(new LexerException(requireNonNull(exception.getLine()),
                                      exception.getLineNo(),
                                      exception.getLinePos(),
                                      exception.getLength(),
                                      exception.getMessage())
                           .initCause(exception)
                           .displayString(), is(message));
    }

    @DataProvider
    public static Object[][] dataExpectEof() {
        return new Object[][] {
                {"# comment\n",
                 listOf(),
                 "Error on line 2 row 1: Expected anything, but got end of file\n" +
                 "\n" +
                 "^"},
                {"\n" +
                 "f00 -1",
                 listOf("f00", "-1"),
                 "Error on line 2 row 7: Expected anything, but got end of file\n" +
                 "f00 -1\n" +
                 "------^"},
        };
    }

    @Test
    @UseDataProvider("dataExpectEof")
    public void dataExpectEof_streaming(String content, List<String> expected, String message)
            throws IOException {
        TestLexer lexer = streaming(content);
        for (String expect : expected) {
            lexer.expect(expect, t -> t.toString().equals(expect));
        }
        try {
            TestToken token = lexer.expect("anything");
            fail("No exception: " + token.toString());
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(message));
                assertThat(e.toString(), endsWith(message));
                assertThat(e.getLine(), is(notNullValue()));
                assertThat(new LexerException(e, e.getMessage()).displayString(), is(message));
                assertThat(new LexerException(e.getMessage()).getMessage(), is(e.getMessage()));
                assertThat(new LexerException(e.getLine(),
                                              e.getLineNo(),
                                              e.getLinePos(),
                                              e.getLength(),
                                              e.getMessage())
                                   .initCause(e)
                                   .displayString(), is(message));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    @Test
    @UseDataProvider("dataExpectEof")
    public void dataExpectEof_preLoaded(String content, List<String> expected, String message)
            throws IOException {
        TestLexer lexer = preLoaded(content);
        for (String expect : expected) {
            lexer.expect(expect, t -> t.toString().equals(expect));
        }
        try {
            TestToken token = lexer.expect("anything");
            fail("No exception: " + token.toString());
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(message));
                assertThat(e.toString(), endsWith(message));
                assertThat(new LexerException(e, e.getMessage()).displayString(), is(message));
                assertThat(new LexerException(e.getMessage()).getMessage(), is(e.getMessage()));
                assertThat(new LexerException(e.getMessage()).displayString(), is(e.getError() + ": " + e.getMessage()));
                assertThat(new LexerException(requireNonNull(e.getLine()),
                                              e.getLineNo(),
                                              e.getLinePos(),
                                              e.getLength(),
                                              e.getMessage())
                                   .initCause(e)
                                   .displayString(), is(message));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    @DataProvider(format = "%m[%i: %p[0..-2]]")
    public static Object[][] lexers() {
        return new Object[][] {
                { "preLoaded", (Function<String, Lexer<TestTokenType, TestToken>>) LexerTest::preLoaded },
                { "streaming", (Function<String, Lexer<TestTokenType, TestToken>>) LexerTest::streaming },
        };
    }

    @Test
    @UseDataProvider("lexers")
    public void testExpect(String name, Function<String, Lexer<TestTokenType, TestToken>> create)
            throws IOException {
        Lexer<TestTokenType, TestToken> lexer =
                create.apply("boo\n" +
                             "foo \"bar\" -5");
        lexer.expect(name + ": identifier 1", TestTokenType.IDENTIFIER);
        assertThat(lexer.peek(), is(notNullValue()));
        assertThat(lexer.peek("foo").type(), is(TestTokenType.IDENTIFIER));
        try {
            lexer.expect("number", TestTokenType.NUMBER);
            fail("expected number");
        } catch (LexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 2 row 1-3: Expected number, but got 'foo'\n" +
                    "foo \"bar\" -5\n" +
                    "^^^"));
            assertThat(e.getLine(), is(notNullValue()));
            assertThat(e.getLine().toString(), is("foo \"bar\" -5"));
            assertThat(e.getLineNo(), is(2));
            assertThat(e.getLinePos(), is(1));
            assertThat(e.getLength(), is(3));
        }

        // Assert that failed expect did not consume the token.
        assertThat(lexer.hasNext(), is(true));
        lexer.expect(name + ": identifier 2", TestTokenType.IDENTIFIER);
        lexer.expect(name + ": string", t -> t.type() == TestTokenType.STRING);
        assertThat(lexer.expect("number").type(), is(TestTokenType.NUMBER));
        assertThat(lexer.hasNext(), is(false));

        try {
            lexer.peek("anything");
            fail("no next");
        } catch (LexerException e) {
            assertThat(e.displayString(), is("Error on line 2 row 13: Expected anything, but got end of file\n" +
                                             "foo \"bar\" -5\n" +
                                             "------------^"));
            assertThat(e.getLine(), is(notNullValue()));
            assertThat(e.getLine(), is("foo \"bar\" -5"));
            assertThat(e.getLineNo(), is(2));
            assertThat(e.getLinePos(), is(13));
            assertThat(e.getLength(), is(1));
        }
    }

    @Test
    @UseDataProvider("lexers")
    public void testExpectSymbol(String name, Function<String, Lexer<TestTokenType, TestToken>> create)
            throws IOException {
        Lexer<TestTokenType, TestToken> lexer =
                create.apply("# comment\n" +
                             "%foo # non-ending");
        try {
            lexer.expectSymbol("foo", '@', '&');
            fail("no exception: " + name);
        } catch (LexerException e) {
            assertThat(e.displayString(), is("Error on line 2 row 1: Expected foo, but got '%'\n" +
                                             "%foo # non-ending\n" +
                                             "^"));
        }
        assertThat(lexer.expectSymbol("percent", '%', '&').isSymbol('%'), is(true));
        try {
            lexer.expectSymbol("foo", '@', '%');
            fail("no exception: " + name);
        } catch (LexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 2 row 2-4: Expected foo, but got 'foo'\n" +
                    "%foo # non-ending\n" +
                    "-^^^"));
        }
        lexer.next();
        try {
            lexer.expectSymbol("foo", '@', '%');
            fail("no exception: " + name);
        } catch (LexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 2 row 19: Expected foo, but got end of file\n" +
                    "%foo # non-ending\n" +
                    "------------------^"));
        }
    }

    @Test
    @UseDataProvider("lexers")
    public void testReadUntil(String name, Function<String, Lexer<TestTokenType, TestToken>> create)
            throws IOException {
        Lexer<TestTokenType, TestToken> lexer =
                create.apply("# comment\n" +
                             "foo(bar\n" +
                             " bar)\n" +
                             "/me\n" +
                             "/ and more\n" +
                             "// and ///");
        lexer.expect(name + ": foo", TestTokenType.IDENTIFIER);
        lexer.expectSymbol(name + ": start", '(');
        TestToken token = lexer.readUntil(")", TestTokenType.GENERIC, true);
        assertThat(token, is(notNullValue()));
        assertThat(token.toString(), is("bar\n bar"));

        lexer.expectSymbol(name + ": multiline", '/');
        TestToken more = lexer.readUntil("///", TestTokenType.IDENTIFIER, false);
        assertThat(more, is(notNullValue()));
        assertThat(more.toString(), is("me\n/ and more\n// and "));
        assertThat(lexer.hasNext(), is(false));

        assertThat(lexer.readUntil("\n", TestTokenType.GENERIC, true), is(nullValue()));
        assertThat(lexer.readUntil("--", TestTokenType.GENERIC, true), is(nullValue()));
    }

    @Test
    @UseDataProvider("lexers")
    public void testIterator(String name, Function<String, Lexer<TestTokenType, TestToken>> create) {
        Lexer<TestTokenType, TestToken> lexer =
                create.apply("# comment\n" +
                             "foo(bar\n" +
                             " bar)");
        List<TestToken> tokens = UnmodifiableList.copyOf(lexer);
        assertThat(tokens.size(), is(5));
        assertThat(tokens.get(0).type(), is(TestTokenType.IDENTIFIER));
        assertThat(tokens.get(1).type(), is(TestTokenType.SYMBOL));
        assertThat(tokens.get(2).type(), is(TestTokenType.IDENTIFIER));
        assertThat(tokens.get(3).type(), is(TestTokenType.IDENTIFIER));
        assertThat(tokens.get(3).toString(), is("bar"));
        assertThat(tokens.get(4).type(), is(TestTokenType.SYMBOL));
        assertThat(tokens.get(4).toString(), is(")"));

        lexer = create.apply("# comment\n" +
                             "foo-\n");
        Iterator<TestToken> iter = lexer.iterator();
        assertThat(iter.hasNext(), is(true));
        assertThat(iter.next().type(), is(TestTokenType.IDENTIFIER));
        try {
            iter.hasNext();
        } catch (UncheckedLexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 2 row 4: No decimal after negative indicator\n" +
                    "foo-\n" +
                    "---^"));
            assertThat(e.toString(), containsString("No decimal after negative indicator"));
        }

        lexer = create.apply("# comment\n" +
                             "foo-\n");
        iter = lexer.iterator();
        assertThat(iter.hasNext(), is(true));
        assertThat(iter.next().type(), is(TestTokenType.IDENTIFIER));
        try {
            iter.next();
        } catch (UncheckedLexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 2 row 4: No decimal after negative indicator\n" +
                    "foo-\n" +
                    "---^"));
            assertThat(e.toString(), containsString("No decimal after negative indicator"));
        }

        assertThat(lexer.toString(), allOf(startsWith("TestLexer{tokenizer=TestTokenizer{preLoaded="),
                                           endsWith("}, next=null}")));
    }

    // ----------------------------
    // ----   HELPER METHODS   ----
    // ----------------------------

    private static TestLexer streaming(String content) {
        StringReader reader = new StringReader(content);
        return new TestLexer(new TestTokenizer(reader, TokenizerBase.DEFAULT_BUFFER_SIZE, false));
    }

    private static TestLexer preLoaded(String content) {
        StringReader reader = new StringReader(content);
        return new TestLexer(new TestTokenizer(reader, TokenizerBase.DEFAULT_BUFFER_SIZE, true));
    }
}
