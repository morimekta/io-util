package net.morimekta.util.lexer.impl;

import net.morimekta.util.lexer.Token;

import javax.annotation.Nonnull;

public class TestToken extends Token<TestTokenType> {
    /**
     * Create a slice instance. The slice is only meant to be internal state
     * immutable, and not representing an immutable byte content.
     *
     * @param fb      The buffer to wrap.
     * @param off     The start offset to wrap.
     * @param len     The length to represent.
     * @param type    The token type represented.
     * @param lineNo  The current line number.
     * @param linePos The current line position.
     */
    public TestToken(char[] fb, int off, int len,
                     @Nonnull TestTokenType type, int lineNo, int linePos) {
        super(fb, off, len, type, lineNo, linePos);
    }
}
