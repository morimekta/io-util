package net.morimekta.util.lexer.impl;

import net.morimekta.util.lexer.Lexer;
import net.morimekta.util.lexer.Tokenizer;

public class TestLexer extends Lexer<TestTokenType, TestToken> {
    /**
     * Create a lexer instance using a specific tokenizer.
     *
     * @param tokenizer The tokenizer to be used to get tokens.
     */
    public TestLexer(Tokenizer<TestTokenType, TestToken> tokenizer) {
        super(tokenizer);
    }
}
