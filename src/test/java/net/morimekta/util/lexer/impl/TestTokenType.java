package net.morimekta.util.lexer.impl;

import net.morimekta.util.lexer.TokenType;

import java.util.Locale;

public enum TestTokenType implements TokenType {
    STRING,
    IDENTIFIER,
    NUMBER,
    SYMBOL,
    GENERIC,
    ;

    @Override
    public String toString() {
        return "<" + name().toLowerCase(Locale.US) + ">";
    }
}
