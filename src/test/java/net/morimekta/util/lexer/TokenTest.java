package net.morimekta.util.lexer;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.util.lexer.impl.TestToken;
import net.morimekta.util.lexer.impl.TestTokenType;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class TokenTest {
    @DataProvider
    public static Object[][] dataLine() {
        return new Object[][] {
                {"foo 500 bar", 4, 5, "foo 500 bar"},
                {"\n" +
                 "foo 500 bar\n" +
                 "more\n" +
                 "not in line", 5, 5, "foo 500 bar"},
                {"500 bar", 0, 5, "·   500 bar"},
                {"500 bar", 0, 3, "  500 bar"},
        };
    }

    @Test
    @UseDataProvider
    public void testLine(String content, int off, int linePos, String line) {
        TestToken token = token(content, off, Math.min(3, content.length() - off), TestTokenType.IDENTIFIER, 2, linePos);
        assertThat(token.line().toString(), is(line));
    }

    @DataProvider
        public static Object[][] dataDecode() {
        return new Object[][] {
                {"ab\\b\\f\\n\\r\\t\\\'\\\"\\\\\\u0020\\040\\0\\177·\007",
                 "ab\b\f\n\r\t'\"\\  \0\177·\007"},
                {"\uD800\uDC97", "\uD800\uDC97"},
                {"\\uD800\uDC97", "\uD800\uDC97"},
                {"\uD800\\uDC97", "\uD800\uDC97"},
                {"\\uD800\\uDC97", "\uD800\uDC97"},
        };
    }

    @Test
    @UseDataProvider
    public void testDecode(String str, String decoded) {
        TestToken token = token("\"" + str + "\"", TestTokenType.STRING);
        assertThat(token.decodeString(false), is(decoded));
        assertThat(token.decodeString(true), is(decoded));
    }

    @DataProvider
    public static Object[][] dataDecode_StrictFail() {
        return new Object[][] {
                {"\\01", "?", "Invalid escaped char: '\\01'"},
                {"\\1fbf", "?f", "Invalid escaped char: '\\1fb'"},
                {"\\12", "?", "Invalid escaped char: '\\12'"},
                {"\\lf", "?f", "Invalid escaped char: '\\l'"},

                {"\\uD800b", "?b", "Unmatched high surrogate char: '\\ud800'"},
                {"\\uD80", "?", "Invalid escaped unicode char: '\\uD80'"},
                {"\\u1-23f", "?f", "Invalid escaped unicode char: '\\u1-23'"},
                {"\uDC97\\uD800f", "??f", "Unmatched low surrogate char: '\\udc97'"},
                {"\uD800", "?", "Unmatched high surrogate char: '\\ud800'"},
                {"\\uD800", "?", "Unmatched high surrogate char: '\\ud800'"},
                {"\\uDC97\\uD800f", "??f", "Unmatched low surrogate char: '\\udc97'"},
                {"\\uD800\\uDC9", "??", "Invalid escaped unicode char: '\\uDC9'"},
                {"\\uD800\\177", "?\177", "Unmatched high surrogate char: '\\ud800'"},
                {"\\uD800\\u1-23", "??", "Invalid escaped unicode char: '\\u1-23'"},
                {"\\ud800\\uD812", "??", "Unmatched high surrogate char: '\\ud800'"},
                };
    }

    @Test
    @UseDataProvider
    public void testDecode_StrictFail(String str, String lenient, String strictMessage) {
        TestToken token = token("\"" + str + "\"", TestTokenType.STRING);
        assertThat(token.decodeString(false), equalTo(lenient));
        try {
            token.decodeString(true);
            fail("no exception: " + strictMessage);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(strictMessage));
        }
    }

    @Test
    public void testIsSymbol() {
        assertThat(token("?", TestTokenType.SYMBOL).isSymbol('?'), is(true));
        assertThat(token("?", TestTokenType.STRING).isSymbol('?'), is(true));
        assertThat(token("??", TestTokenType.SYMBOL).isSymbol('?'), is(false));
        assertThat(token("??", TestTokenType.STRING).isSymbol('?'), is(false));
    }

    @Test
    public void testObject() {
        TestToken t = token("foo", TestTokenType.STRING);
        assertThat(token("foo", TestTokenType.STRING), is(t));
        assertThat(token("foo", TestTokenType.SYMBOL), is(not(t)));
        assertThat(t, is(t));
        assertThat(t, is(not("foo")));

        assertThat(t.hashCode(), is(token("foo", TestTokenType.STRING).hashCode()));
        assertThat(t.hashCode(), is(not(token("bar", TestTokenType.STRING).hashCode())));
    }

    public TestToken token(String str, TestTokenType type) {
        return token(str, 0, str.length(), type, 1, 1);
    }

    public TestToken token(String str, int start, int len, TestTokenType type, int lineNo, int linePos) {
        return new TestToken(str.toCharArray(), start, len, type, lineNo, linePos);
    }
}
