package net.morimekta.util.collect;

import org.junit.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Comparator.reverseOrder;
import static net.morimekta.util.collect.Unmodifiables.toSortedMap;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

public class UnmodifiableSortedMapTest {
    @Test
    public void testConvenience() {
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s)).keySet(),
                   contains("a", "b", "c"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s, reverseOrder())).keySet(),
                   contains("c", "b", "a"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s,
                                                                s -> "" + ((char) (s.charAt(0) + 1)))).values(),
                   contains("b", "c", "d"));
        assertThat(Stream.of("a", "b", "c").collect(toSortedMap(s -> s,
                                                                s -> "" + ((char) (s.charAt(0) + 1)),
                                                                reverseOrder())).values(),
                   contains("d", "c", "b"));

        assertThat(IntStream.range(0, 4096).parallel().boxed().collect(toSortedMap(s -> s)).keySet(),
                   hasSize(4096));
    }

    @Test
    public void testBaseMap() {
        assertThat(UnmodifiableSortedMap.sortedMapOf().toString(), is("{}"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("bar", "foo").toString(), is("{bar: foo}"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("bar", "foo", "foo", "bar").toString(), is("{bar: foo, foo: bar}"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("bar", "foo").hashCode(), is(UnmodifiableSortedMap.sortedMapOf("bar", "foo").hashCode()));
        assertThat(UnmodifiableSortedMap.sortedMapOf("bar", "foo").hashCode(), is(not(UnmodifiableSortedMap.sortedMapOf("bar", "bar").hashCode())));

        assertThat(UnmodifiableSortedMap.sortedMapOf().equals(UnmodifiableSortedMap.sortedMapOf()), is(true));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(new HashMap<String,String>(){{put("foo", "bar");}}), is(true));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(new HashMap<String,String>(){{put("foo", "foo");}}), is(false));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(new HashMap<String,String>(){{put("foo", null);}}), is(false));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(new HashMap<String,String>(){{put("bar", "bar");}}), is(false));

        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(UnmodifiableSortedMap.sortedMapOf("foo", "bar")), is(true));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(UnmodifiableSortedMap.sortedMapOf("foo", "foo")), is(false));
        assertThat(UnmodifiableSortedMap.sortedMapOf("foo", "bar").equals(UnmodifiableSortedMap.sortedMapOf("bar", "bar")), is(false));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(UnmodifiableSortedMap.sortedMapOf(), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(UnmodifiableSortedMap.sortedMapOf().entrySet(), is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(UnmodifiableSortedMap.sortedMapOf().values(), hasSize(0));
        assertThat(UnmodifiableSortedMap.sortedMapOf().isEmpty(), is(true));

        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a").keySet(),
                   contains("a"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b").keySet(),
                   contains("a", "b"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c").keySet(),
                   contains("a", "b", "c"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d").keySet(),
                   contains("a", "b", "c", "d"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e").keySet(),
                   contains("a", "b", "c", "d", "e"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                                                     "f", "f").keySet(),
                   contains("a", "b", "c", "d", "e", "f"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                                                     "f", "f", "g", "g").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                                                     "f", "f", "g", "g", "h", "h").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                                                     "f", "f", "g", "g", "h", "h", "i", "i").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i"));
        assertThat(UnmodifiableSortedMap.sortedMapOf("a", "a", "b", "b", "c", "c", "d", "d", "e", "e",
                                                     "f", "f", "g", "g", "h", "h", "i", "i", "j", "j").keySet(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i", "j"));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(UnmodifiableSortedMap.copyOf(new HashMap<>()),
                   is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        Map<String,String> tmp = UnmodifiableSortedMap.sortedMapOf("a", "b");
        assertThat(UnmodifiableSortedMap.copyOf(tmp), is(sameInstance(tmp)));

        Map<String,String> other;
        other = new TreeMap<>(reverseOrder());
        other.putAll(tmp);
        assertThat(UnmodifiableSortedMap.copyOf(other), is(tmp));
        assertThat(UnmodifiableSortedMap.copyOf(other).comparator(), is(reverseOrder()));
        other = new HashMap<>(tmp);
        assertThat(UnmodifiableSortedMap.copyOf(other), is(tmp));
    }

    @Test
    public void testBuilder() {
        UnmodifiableSortedMap.Builder<String,String> builder = UnmodifiableSortedMap.builderNaturalOrder(5);
        builder.putAll(UnmodifiableSortedMap.sortedMapOf("A", "B"));
        builder.put("B", "NOT")
               .put("B", "C");
        Map<String, String> instance = builder.build();
        assertThat(builder.build(), is(sameInstance(instance)));
        assertThat(instance.size(), is(2));
        assertThat(instance, hasEntry("A", "B"));
        assertThat(instance, hasEntry("B", "C"));
        assertThat(instance.get("A"), is("B"));
        assertThat(instance.get("C"), is(nullValue()));

        builder.put("B", "MORE");

        assertThat(UnmodifiableSortedMap.builderNaturalOrder().build(), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(UnmodifiableSortedMap.builderNaturalOrder(5).build(), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(UnmodifiableSortedMap.builderReverseOrder().build(), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(UnmodifiableSortedMap.builderReverseOrder(5).build(), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));

        Comparator<String> comparator = Comparator.comparing(String::length);
        assertThat(UnmodifiableSortedMap.<String,String>builderOrderedBy(comparator)
                           .put("a", "b").build().comparator(), is(comparator));
        assertThat(UnmodifiableSortedMap.<String,String>builderOrderedBy(1, comparator)
                           .put("a", "b").build().comparator(), is(comparator));
        assertThat(UnmodifiableSortedMap.<String,String>builderNaturalOrder()
                           .put("a", "b").build().comparator(), is(nullValue()));
        assertThat(UnmodifiableSortedMap.<String,String>builderNaturalOrder(1)
                           .put("a", "b").build().comparator(), is(nullValue()));
        assertThat(UnmodifiableSortedMap.<String,String>builderReverseOrder()
                           .put("a", "b").build().comparator(), is(reverseOrder()));
        assertThat(UnmodifiableSortedMap.<String,String>builderReverseOrder(1)
                           .put("a", "b").build().comparator(), is(reverseOrder()));

        for (int i = 0; i < 1050; ++i) {
            builder.put("foo", UUID.randomUUID().toString());
            builder.put("bar", UUID.randomUUID().toString());
            builder.put("baz", UUID.randomUUID().toString());
        }

        assertThat(builder.build().keySet(),
                   contains("A", "B", "bar", "baz", "foo"));
        assertThat(builder.build().size, is(5));
    }

    @Test
    public void testSortedMap() {
        SortedMap<String, String> map = UnmodifiableSortedMap.sortedMapOf("b", "b", "d", "d", "f", "f");
        assertThat(map.subMap("a", "c").keySet(), contains("b"));
        assertThat(map.subMap("b", "c").keySet(), contains("b"));
        assertThat(map.subMap("f", "g").keySet(), contains("f"));
        assertThat(map.subMap("g", "g"), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(map.subMap("a", "g"), is(sameInstance(map)));

        assertThat(map.tailMap("a"), is(sameInstance(map)));
        assertThat(map.tailMap("b"), is(sameInstance(map)));
        assertThat(map.tailMap("c").keySet(), contains("d", "f"));
        assertThat(map.tailMap("d").keySet(), contains("d", "f"));
        assertThat(map.tailMap("e").keySet(), contains("f"));
        assertThat(map.tailMap("f").keySet(), contains("f"));
        assertThat(map.tailMap("g"), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));

        assertThat(map.headMap("a"), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(map.headMap("b"), is(sameInstance(UnmodifiableSortedMap.sortedMapOf())));
        assertThat(map.headMap("c").keySet(), contains("b"));
        assertThat(map.headMap("d").keySet(), contains("b"));
        assertThat(map.headMap("e").keySet(), contains("b", "d"));
        assertThat(map.headMap("f").keySet(), contains("b", "d"));
        assertThat(map.headMap("g"), is(sameInstance(map)));

        assertThat(map.firstKey(), is("b"));
        assertThat(map.lastKey(), is("f"));
        assertThat(UnmodifiableSortedMap.sortedMapOf().firstKey(), is(nullValue()));
        assertThat(UnmodifiableSortedMap.sortedMapOf().lastKey(), is(nullValue()));

        assertThat(map.containsKey("a"), is(false));
        assertThat(map.containsKey("b"), is(true));
        assertThat(UnmodifiableSortedMap.sortedMapOf().containsKey("c"), is(false));
    }

    @Test
    public void testUnsupported_Map() {
        try {
            UnmodifiableSortedMap.sortedMapOf().put(new Object(), new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedMap.sortedMapOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedMap.sortedMapOf().putAll(new HashMap<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedMap.sortedMapOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
