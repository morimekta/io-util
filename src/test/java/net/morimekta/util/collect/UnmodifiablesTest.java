package net.morimekta.util.collect;

import org.junit.Test;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static net.morimekta.util.collect.Unmodifiables.asSortedSet;
import static net.morimekta.util.collect.Unmodifiables.groupingBy;
import static net.morimekta.util.collect.Unmodifiables.groupingByAll;
import static net.morimekta.util.collect.Unmodifiables.mapToList;
import static net.morimekta.util.collect.Unmodifiables.mapToSet;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class UnmodifiablesTest {
    @Test
    public void testMapToSet() {
        assertThat(mapToSet(listOf(1, 2, 3, 4, 5),
                            i -> Math.pow(i.doubleValue(), 2.0)),
                   is(setOf(1.0, 4.0, 9.0, 16.0, 25.0)));
        assertThat(mapToSet(asSortedSet(1, 2, 3, 4, 5), i -> 5 - i),
                   // it get's sorted
                   contains(0, 1, 2, 3, 4));
    }

    @Test
    public void testMapToList() {
        assertThat(mapToList(asSortedSet(1, 2, 3, 4, 5), i -> 5 - i),
                   contains(4, 3, 2, 1, 0));
    }

    @Test
    public void testGroupingBy() {
        assertThat(listOf("Donald Trump",
                          "Barack Obama",
                          "George W. Bush",
                          "Bill Clinton",
                          "George H. W. Bush")
                           .stream()
                           .parallel()
                           .collect(groupingBy(k -> k.replaceAll(".* ", ""))),
                   is(mapOf("Trump", listOf("Donald Trump"),
                            "Obama", listOf("Barack Obama"),
                            "Clinton", listOf("Bill Clinton"),
                            "Bush", listOf("George W. Bush", "George H. W. Bush"))));
    }

    @Test
    public void testGroupingByAll() {
        assertThat(listOf("Donald Trump",
                          "Barack Obama",
                          "George W. Bush",
                          "Bill Clinton",
                          "George H. W. Bush")
                           .stream()
                           .parallel()
                           .collect(groupingByAll(k -> listOf(
                                   k.replaceAll(".* ", ""),
                                   k.replaceAll(" .*", "")))),
                   is(mapOf(
                           "Donald", listOf("Donald Trump"),
                           "Trump", listOf("Donald Trump"),
                           "Barack", listOf("Barack Obama"),
                           "Obama", listOf("Barack Obama"),
                           "Clinton", listOf("Bill Clinton"),
                           "Bill", listOf("Bill Clinton"),
                           "George", listOf("George W. Bush", "George H. W. Bush"),
                           "Bush", listOf("George W. Bush", "George H. W. Bush"))));
    }
}
