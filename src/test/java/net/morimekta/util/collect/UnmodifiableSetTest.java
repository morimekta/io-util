package net.morimekta.util.collect;

import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static net.morimekta.util.collect.Unmodifiables.asSet;
import static net.morimekta.util.collect.Unmodifiables.toSet;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

public class UnmodifiableSetTest {
    @Test
    public void testConvenience() {
        assertThat(asSet("a", "b"),
                   is(UnmodifiableSet.setOf("a", "b")));
        assertThat(IntStream.range(2, 5).boxed().collect(toSet()),
                   is(UnmodifiableSet.setOf(2, 3, 4)));
        assertThat(IntStream.range(0, 4096).parallel().boxed().collect(toSet()),
                   hasSize(4096));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(UnmodifiableSet.setOf(), is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.setOf().equals(UnmodifiableSet.setOf()), is(true));
        assertThat(UnmodifiableSet.setOf().asList(), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableSet.setOf(), hasSize(0));
        assertThat(UnmodifiableSet.setOf().isEmpty(), is(true));
        assertThat(UnmodifiableSet.setOf().iterator().hasNext(), is(false));
        assertThat(UnmodifiableSet.setOf().contains(new Object()), is(false));
        assertThat(UnmodifiableSet.setOf().contains(null), is(false));
        assertThat(UnmodifiableSet.setOf("foo").equals(new HashSet<String>(){{add("foo");}}), is(true));
        assertThat(UnmodifiableSet.setOf("foo").equals(new ArrayList<String>(){{add("foo");}}), is(false));
        assertThat(UnmodifiableSet.setOf("foo", "bar").equals(new HashSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSet.setOf("bar", "foo").equals(new HashSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSet.setOf("foo", "bar").equals(new TreeSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSet.setOf("bar", "foo").equals(new TreeSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSet.setOf("bar", "foo").toString(), is("{bar, foo}"));
        assertThat(UnmodifiableSet.setOf("bar", "foo").hashCode(), is(UnmodifiableSet.setOf("bar", "foo").hashCode()));
        assertThat(UnmodifiableSet.setOf("bar", "foo").hashCode(), is(not(UnmodifiableSet.setOf("bar", "bar").hashCode())));
        assertThat(UnmodifiableSet.setOf("foo").contains("foo"), is(true));
        assertThat(UnmodifiableSet.setOf("foo"), contains("foo"));
        assertThat(UnmodifiableSet.setOf("foo", "bar"),
                   contains("foo", "bar"));
        assertThat(UnmodifiableSet.setOf("foo", "bar").asList(),
                   contains("foo", "bar"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz"),
                   contains("foo", "bar", "baz"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo"),
                   contains("foo", "bar", "baz", "boo"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin"),
                   contains("foo", "bar", "baz", "boo", "bin"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"));
        assertThat(UnmodifiableSet.setOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"));
        assertThat(UnmodifiableSet.setOf("a", "b", "c").toArray(new String[0]),
                   is(new String[]{"a", "b", "c"}));
    }

    @Test
    public void testContains() {
        String uuid = UUID.randomUUID().toString();
        UnmodifiableSet<String> set = UnmodifiableSet.setOf(
                uuid, UUID.randomUUID().toString());

        assertThat(set.contains(uuid), is(true));
        assertThat(set.contains(UUID.randomUUID().toString()), is(false));

        assertThat(UnmodifiableSet.setOf(UUID.randomUUID()).contains(UUID.randomUUID()), is(false));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(UnmodifiableSet.copyOf(new Object[0]),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf(Collections.emptyEnumeration()),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf(Collections.emptyIterator()),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf((Iterable<?>) Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf(Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf((Iterable<?>) UnmodifiableList.listOf()),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf(UnmodifiableList.listOf()),
                   is(sameInstance(UnmodifiableSet.setOf())));
        assertThat(UnmodifiableSet.copyOf(Iterables.unmodifiableIterable(new LinkedList<>())),
                   is(sameInstance(UnmodifiableSet.setOf())));

        assertThat(UnmodifiableSet.copyOf(new String[]{"a", "b", "c"}),
                   is(UnmodifiableSet.setOf("a", "b", "c")));
        assertThat(UnmodifiableSet.copyOf(Collections.enumeration(UnmodifiableList.listOf("a", "b", "c"))),
                   is(UnmodifiableSet.setOf("a", "b", "c")));
        assertThat(UnmodifiableSet.copyOf(UnmodifiableList.listOf("a", "b", "c")),
                   is(UnmodifiableSet.setOf("a", "b", "c")));
        assertThat(UnmodifiableSet.copyOf(Arrays.asList("a", "b", "c")),
                   is(UnmodifiableSet.setOf("a", "b", "c")));
        assertThat(UnmodifiableSet.copyOf(UnmodifiableList.listOf("a", "b", "c").iterator()),
                   is(UnmodifiableSet.setOf("a", "b", "c")));
    }

    @Test
    public void testBuilder() {
        UnmodifiableSet.Builder<String> builder = UnmodifiableSet.builder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        assertThat(builder.build(),
                   contains("a", "c", "b", "e", "f", "g", "d", "i"));
    }

    @Test
    public void testUnsupported_Collection() {
        try {
            UnmodifiableSet.setOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSet.setOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
