package net.morimekta.util.collect;

import org.junit.Test;

import static net.morimekta.util.Pair.pair;
import static net.morimekta.util.collect.SetOperations.difference;
import static net.morimekta.util.collect.SetOperations.disjoint;
import static net.morimekta.util.collect.SetOperations.intersect;
import static net.morimekta.util.collect.SetOperations.product;
import static net.morimekta.util.collect.SetOperations.subset;
import static net.morimekta.util.collect.SetOperations.subtract;
import static net.morimekta.util.collect.SetOperations.union;
import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static net.morimekta.util.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class SetOperationsTest {
    @Test
    public void testUnion() {
        assertThat(union(), is(setOf()));
        assertThat(union(setOf(2, 4, 6)), is(setOf(2, 4, 6)));
        assertThat(union(setOf(1, 2, 3), setOf(2, 3, 4), setOf(3, 4, 5)),
                   is(setOf(1, 2, 3, 4, 5)));
        assertThat(union(sortedSetOf(5, 4, 3), setOf(1, 2, 3)),
                   is(sortedSetOf(1, 2, 3, 4, 5)));
        assertThat(union(sortedSetOf(5, 4, 3), setOf(1, 2, 3)),
                   is(instanceOf(UnmodifiableSortedSet.class)));
    }

    @Test
    public void testIntersect() {
        assertThat(intersect(setOf(1, 2, 3), setOf(2, 3, 4)),
                   is(instanceOf(UnmodifiableSet.class)));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(2, 3, 4)),
                   is(instanceOf(UnmodifiableSortedSet.class)));

        assertThat(intersect(setOf(1, 2)), is(setOf(1, 2)));
        assertThat(intersect(setOf(), setOf(1, 2, 3)), is(setOf()));
        assertThat(intersect(setOf(1, 2, 3), setOf(2, 3, 4)), is(setOf(2, 3)));
        assertThat(intersect(setOf(1, 2, 3), setOf(4, 5, 6)), is(setOf()));

        assertThat(intersect(sortedSetOf(1, 2)), is(setOf(1, 2)));
        assertThat(intersect(sortedSetOf(), setOf(1, 2, 3)), is(setOf()));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(2, 3, 4)), is(setOf(2, 3)));
        assertThat(intersect(sortedSetOf(1, 2, 3), setOf(4, 5, 6)), is(setOf()));
    }

    @Test
    public void testSubtract() {
        assertThat(subtract(setOf(1, 2)), is(instanceOf(UnmodifiableSet.class)));
        assertThat(subtract(sortedSetOf(1, 2)), is(instanceOf(UnmodifiableSortedSet.class)));

        assertThat(subtract(setOf()), is(setOf()));
        assertThat(subtract(setOf(1, 2, 3)), is(setOf(1, 2, 3)));
        assertThat(subtract(setOf(1, 2, 3), setOf(3, 4, 5)), is(setOf(1, 2)));
        assertThat(subtract(sortedSetOf()), is(sortedSetOf()));
        assertThat(subtract(sortedSetOf(1, 2, 3)), is(sortedSetOf(1, 2, 3)));
        assertThat(subtract(sortedSetOf(1, 2, 3), setOf(3, 4, 5)), is(sortedSetOf(1, 2)));
    }

    @Test
    public void testDifference() {
        assertThat(difference(sortedSetOf(), setOf()), is(instanceOf(UnmodifiableSortedSet.class)));
        assertThat(difference(setOf(), sortedSetOf()), is(instanceOf(UnmodifiableSet.class)));

        assertThat(difference(sortedSetOf(1, 2), setOf()), is(setOf(1, 2)));
        assertThat(difference(sortedSetOf(), setOf(1, 2)), is(setOf(1, 2)));
        assertThat(difference(setOf(1, 2), setOf()), is(setOf(1, 2)));
        assertThat(difference(setOf(), setOf(1, 2)), is(setOf(1, 2)));

        assertThat(difference(setOf(1, 2, 3), setOf(2, 3, 4)),
                   is(setOf(1, 4)));
        assertThat(difference(sortedSetOf(1, 2, 3), setOf(2, 3, 4)),
                   is(setOf(1, 4)));
    }

    @Test
    public void testProduct() {
        assertThat(product(sortedSetOf(1, 2), sortedSetOf()), is(sortedSetOf()));
        assertThat(product(sortedSetOf(), sortedSetOf(1, 2)), is(sortedSetOf()));
        assertThat(product(sortedSetOf(5, 1, 3), sortedSetOf("B", "A")),
                   contains(pair(1, "A"),
                            pair(1, "B"),
                            pair(3, "A"),
                            pair(3, "B"),
                            pair(5, "A"),
                            pair(5, "B")));

        assertThat(product(setOf(1, 2), setOf()), is(setOf()));
        assertThat(product(setOf(), setOf(1, 2)), is(setOf()));
        assertThat(product(setOf(5, 1, 3), setOf("B", "A")),
                   contains(pair(5, "B"),
                            pair(5, "A"),
                            pair(1, "B"),
                            pair(1, "A"),
                            pair(3, "B"),
                            pair(3, "A")));
    }

    @Test
    public void testSubset() {
        assertThat(subset(setOf(), setOf(1, 2)), is(true));
        assertThat(subset(setOf(1, 2), setOf(1, 2)), is(true));
        assertThat(subset(setOf(1, 2, 3), setOf(1, 2)), is(false));
        assertThat(subset(setOf(1, 5), setOf(1, 2, 3)), is(false));
    }

    @Test
    public void testDisjoint() {
        assertThat(disjoint(setOf(1, 2), setOf(3, 4)), is(true));
        assertThat(disjoint(setOf(1, 2), setOf(2, 3)), is(false));
        assertThat(disjoint(setOf(1, 2), setOf(1, 2)), is(false));
        assertThat(disjoint(setOf(1, 2), setOf(1, 2, 3)), is(false));
    }
}
