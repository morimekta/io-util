package net.morimekta.util.collect;

import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static java.util.Comparator.reverseOrder;
import static net.morimekta.util.collect.UnmodifiableSortedSet.copyOf;
import static net.morimekta.util.collect.Unmodifiables.asSortedSet;
import static net.morimekta.util.collect.Unmodifiables.toSortedSet;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

public class UnmodifiableSortedSetTest {
    @Test
    public void testConvenience() {
        assertThat(asSortedSet("a", "c", "b"),
                   contains("a", "b", "c"));
        assertThat(asSortedSet(reverseOrder(), "a", "c", "b"),
                   contains("c", "b", "a"));
        assertThat(IntStream.range(2, 5).boxed().collect(toSortedSet()),
                   contains(2, 3, 4));
        assertThat(IntStream.range(2, 5).boxed()
                            .collect(toSortedSet(reverseOrder())),
                   contains(4, 3, 2));

        assertThat(IntStream.range(0, 8192).parallel().boxed().collect(toSortedSet()),
                   hasSize(8192));
    }

    @Test
    public void testConstructor_Of() {
        assertThat(UnmodifiableSortedSet.sortedSetOf(), is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(UnmodifiableSortedSet.sortedSetOf().equals(UnmodifiableSortedSet.sortedSetOf()), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf().asList(), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableSortedSet.sortedSetOf(), hasSize(0));
        assertThat(UnmodifiableSortedSet.sortedSetOf().isEmpty(), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf().iterator().hasNext(), is(false));
        assertThat(UnmodifiableSortedSet.sortedSetOf().contains(new Object()), is(false));
        assertThat(UnmodifiableSortedSet.sortedSetOf().contains(null), is(false));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo").equals(new HashSet<String>(){{add("foo");}}), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo").equals(new ArrayList<String>(){{add("foo");}}), is(false));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar").equals(new HashSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("bar", "foo").equals(new HashSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar").equals(new TreeSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("bar", "foo").equals(new TreeSet<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("bar", "foo").toString(), is("{bar, foo}"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("bar", "foo").hashCode(), is(UnmodifiableSortedSet.sortedSetOf("bar", "foo").hashCode()));
        assertThat(UnmodifiableSortedSet.sortedSetOf("bar", "foo").hashCode(), is(not(UnmodifiableSortedSet.sortedSetOf("bar", "bar").hashCode())));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo").contains("foo"), is(true));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo"), contains("foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar"),
                   contains("bar", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar").asList(),
                   contains("bar", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz"),
                   contains("bar", "baz", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo"),
                   contains("bar", "baz", "boo", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin"),
                   contains("bar", "baz", "bin", "boo", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("bar", "baz", "bin", "ble", "boo", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee", "noo"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("bar", "baz", "bin", "ble", "boo", "bwa", "foo", "nee", "noo", "now"));
        assertThat(UnmodifiableSortedSet.sortedSetOf("c", "a", "b").toArray(new String[0]),
                   is(new String[]{"a", "b", "c"}));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(copyOf(new Object[0]),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf(Collections.emptyEnumeration()),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf(Collections.emptyIterator()),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf((Iterable<?>) Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf(Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf((Iterable<?>) UnmodifiableList.listOf()),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf(UnmodifiableList.listOf()),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
        assertThat(copyOf(Iterables.unmodifiableIterable(new LinkedList<>())),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));

        Set<String> from = asSortedSet("a", "b", "c");
        assertThat(copyOf(from), is(sameInstance(from)));
        Set<String> from2 = new TreeSet<>(reverseOrder());
        from2.addAll(from);
        assertThat(copyOf(from2), is(from));
        assertThat(copyOf(from2).comparator(), is(reverseOrder()));
        assertThat(copyOf(new String[]{"a", "b", "c"}),
                   is(UnmodifiableSortedSet.sortedSetOf("a", "b", "c")));
        assertThat(copyOf(Collections.enumeration(UnmodifiableList.listOf("a", "b", "c"))),
                   is(UnmodifiableSortedSet.sortedSetOf("a", "b", "c")));
        assertThat(copyOf(UnmodifiableList.listOf("a", "b", "c")),
                   is(UnmodifiableSortedSet.sortedSetOf("a", "b", "c")));
        assertThat(copyOf(Arrays.asList("a", "b", "c")),
                   is(UnmodifiableSortedSet.sortedSetOf("a", "b", "c")));
        assertThat(copyOf(UnmodifiableList.listOf("a", "b", "c").iterator()),
                   is(UnmodifiableSortedSet.sortedSetOf("a", "b", "c")));
    }

    @Test
    public void testBuilder_Natural() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.builderNaturalOrder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        Set<String> first = builder.build();
        assertThat(first,
                   contains("a", "b", "c", "d", "e", "f", "g", "i"));
        assertThat(builder.build(), is(sameInstance(first)));
        builder.add("h");
        assertThat(builder.build(),
                   contains("a", "b", "c", "d", "e", "f", "g", "h", "i"));
        assertThat(first,
                   contains("a", "b", "c", "d", "e", "f", "g", "i"));

        assertThat(UnmodifiableSortedSet.builderNaturalOrder().build(),
                   is(sameInstance(UnmodifiableSortedSet.sortedSetOf())));
    }

    @Test
    public void testBuilder_Reverse() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.builderReverseOrder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        Set<String> first = builder.build();
        assertThat(first,
                   contains("i", "g", "f", "e", "d", "c", "b", "a"));
        assertThat(builder.build(), is(sameInstance(first)));
        builder.add("h");
        assertThat(builder.build(),
                   contains("i", "h", "g", "f", "e", "d", "c", "b", "a"));
        assertThat(first,
                   contains("i", "g", "f", "e", "d", "c", "b", "a"));

        assertThat(UnmodifiableSortedSet.builderReverseOrder().build().comparator(),
                   is(Comparator.reverseOrder()));
    }

    @Test
    public void testSortedSet() {
        SortedSet<String> test = UnmodifiableSortedSet.sortedSetOf("b", "d", "f");
        SortedSet<String> expected = new TreeSet<String>(){{add("b");add("d");add("f");}};
        assertThat(test.comparator(), is(nullValue()));
        assertThat(test.subSet("c", "e"), is(expected.subSet("c", "e")));
        assertThat(test.subSet("b", "d"), is(expected.subSet("b", "d")));
        assertThat(test.subSet("a", "f"), is(expected.subSet("a", "f")));
        assertThat(test.subSet("b", "e"), is(expected.subSet("b", "e")));

        assertThat(test.headSet("a"), is(expected.headSet("a")));
        assertThat(test.headSet("b"), is(expected.headSet("b")));
        assertThat(test.headSet("c"), is(expected.headSet("c")));
        assertThat(test.headSet("e"), is(expected.headSet("e")));
        assertThat(test.headSet("f"), is(expected.headSet("f")));
        assertThat(test.headSet("g"), is(expected.headSet("g")));

        assertThat(test.tailSet("a"), is(expected.tailSet("a")));
        assertThat(test.tailSet("b"), is(expected.tailSet("b")));
        assertThat(test.tailSet("c"), is(expected.tailSet("c")));
        assertThat(test.tailSet("e"), is(expected.tailSet("e")));
        assertThat(test.tailSet("f"), is(expected.tailSet("f")));
        assertThat(test.tailSet("g"), is(expected.tailSet("g")));

        assertThat(test.first(), is("b"));
        assertThat(test.last(), is("f"));
        assertThat(UnmodifiableSortedSet.sortedSetOf().first(), is(nullValue()));
        assertThat(UnmodifiableSortedSet.sortedSetOf().last(), is(nullValue()));
    }

    @Test
    public void testMakeCompact() {
        UnmodifiableSortedSet.Builder<String> builder = UnmodifiableSortedSet.builderNaturalOrder();
        for (int i = 0; i < 2050; ++i) {
            builder.addAll("a", "b", "c");
        }
        UnmodifiableSortedSet<String> result = builder.build();
        assertThat(result.array.length, is(3));
        assertThat(result, contains("a", "b", "c"));
    }


    @Test
    public void testUnsupported_Collection() {
        try {
            UnmodifiableSortedSet.sortedSetOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableSortedSet.sortedSetOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
