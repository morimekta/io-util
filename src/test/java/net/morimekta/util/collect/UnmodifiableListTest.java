package net.morimekta.util.collect;

import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static net.morimekta.util.collect.Unmodifiables.asList;
import static net.morimekta.util.collect.Unmodifiables.toList;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

public class UnmodifiableListTest {
    @Test
    public void testSimpleList_Of() {
        assertThat(UnmodifiableList.listOf(), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.listOf().equals(UnmodifiableList.listOf().asList()), is(true));
        assertThat(UnmodifiableList.listOf().asList(), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.listOf(), hasSize(0));
        assertThat(UnmodifiableList.listOf().isEmpty(), is(true));
        assertThat(UnmodifiableList.listOf().iterator().hasNext(), is(false));
        assertThat(UnmodifiableList.listOf().contains(new Object()), is(false));
        assertThat(UnmodifiableList.listOf().contains(null), is(false));
        assertThat(UnmodifiableList.listOf("foo").equals(new HashSet<String>(){{add("foo");}}), is(false));
        assertThat(UnmodifiableList.listOf("foo").equals(new ArrayList<String>()), is(false));
        assertThat(UnmodifiableList.listOf("foo", "bar").equals(new ArrayList<String>(){{add("foo");add("foo");}}), is(false));
        assertThat(UnmodifiableList.listOf("foo", "bar").equals(new ArrayList<String>(){{add("foo");add("bar");}}), is(true));
        assertThat(UnmodifiableList.listOf("bar", "bar").equals(new LinkedList<String>(){{add("bar");add("bar");}}), is(true));
        assertThat(UnmodifiableList.listOf("bar", "foo").equals(new LinkedList<String>(){{add("bar");add("bar");}}), is(false));
        assertThat(UnmodifiableList.listOf("bar", "foo").toString(), is("[bar, foo]"));
        assertThat(UnmodifiableList.listOf("bar", "foo").hashCode(), is(UnmodifiableList.listOf("bar", "foo").hashCode()));
        assertThat(UnmodifiableList.listOf("bar", "foo").hashCode(), is(not(UnmodifiableList.listOf("bar", "bar").hashCode())));
        assertThat(UnmodifiableList.listOf("foo").contains("foo"), is(true));
        assertThat(UnmodifiableList.listOf("foo"), contains("foo"));
        assertThat(UnmodifiableList.listOf("foo", "bar"),
                   contains("foo", "bar"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz"),
                   contains("foo", "bar", "baz"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo"),
                   contains("foo", "bar", "baz", "boo"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin"),
                   contains("foo", "bar", "baz", "boo", "bin"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin", "ble"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo"));
        assertThat(UnmodifiableList.listOf("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"),
                   contains("foo", "bar", "baz", "boo", "bin", "ble", "bwa", "nee", "noo", "now"));
        assertThat(UnmodifiableList.listOf("a", "b", "c").toArray(new String[0]),
                   is(new String[]{"a", "b", "c"}));
        assertThat(UnmodifiableList.listOf("a", "b", "c").get(2), is("c"));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").indexOf("c"), is(1));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").indexOf("y"), is(-1));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").lastIndexOf("c"), is(3));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").lastIndexOf("y"), is(-1));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").listIterator().next(), is("a"));
        assertThat(UnmodifiableList.listOf("a", "c", "b", "c", "b").listIterator(2).next(), is("b"));
        assertThat(UnmodifiableList.listOf("a", "b").subList(0, 1), is(UnmodifiableList.listOf("a")));
        assertThat(UnmodifiableList.listOf("a", "b").subList(1, 2), is(UnmodifiableList.listOf("b")));
        UnmodifiableList<String> tmp = UnmodifiableList.listOf("a", "c", "b", "c", "b");
        assertThat(tmp.subList(0, 5), is(sameInstance(tmp)));
        assertThat(tmp.subList(2, 3), is(tmp.subList(2, 3)));
    }

    @Test
    public void testConstructor_CopyOf() {
        assertThat(UnmodifiableList.copyOf(new Object[0]),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf(Collections.emptyEnumeration()),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf(Collections.emptyIterator()),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf((Iterable<?>) Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf(Collections.EMPTY_LIST),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf((Iterable<?>) UnmodifiableSet.setOf()),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf(UnmodifiableSet.setOf()),
                   is(sameInstance(UnmodifiableList.listOf())));
        assertThat(UnmodifiableList.copyOf(Iterables.unmodifiableIterable(new LinkedList<>())),
                   is(sameInstance(UnmodifiableList.listOf())));

        assertThat(UnmodifiableList.copyOf(new String[]{"a", "b", "c"}),
                   is(UnmodifiableList.listOf("a", "b", "c")));
        assertThat(UnmodifiableList.copyOf(new String[]{"a", "b", "c"}),
                   is(not(UnmodifiableList.listOf("a", "c", "b"))));
        assertThat(UnmodifiableList.copyOf(Collections.enumeration(UnmodifiableSet.setOf("a", "b", "c"))),
                   is(UnmodifiableList.listOf("a", "b", "c")));
        assertThat(UnmodifiableList.copyOf(UnmodifiableList.listOf("a", "b", "c")),
                   is(UnmodifiableList.listOf("a", "b", "c")));
        assertThat(UnmodifiableList.copyOf(Arrays.asList("a", "b", "c")),
                   is(UnmodifiableList.listOf("a", "b", "c")));
        assertThat(UnmodifiableList.copyOf(UnmodifiableList.listOf("a", "b", "c").iterator()),
                   is(UnmodifiableList.listOf("a", "b", "c")));
    }

    @Test
    public void testCollector() {
        assertThat(asList("a", "b", "c"), is(UnmodifiableList.listOf("a", "b", "c")));
        UnmodifiableList<Integer> collected = IntStream.range(2, 5).boxed().collect(toList());
        assertThat(collected, is(UnmodifiableList.listOf(2, 3, 4)));
        collected = IntStream.range(0, 4096).parallel().boxed().collect(toList());
        assertThat(collected, hasSize(4096));
    }

    @Test
    public void testBuilder() {
        UnmodifiableList.Builder<String> builder = UnmodifiableList.builder(5);
        assertThat(builder.addAll(Arrays.asList("a", "c")), is(sameInstance(builder)));
        assertThat(builder.add("b"), is(sameInstance(builder)));
        assertThat(builder.addAll(), is(sameInstance(builder)));
        assertThat(builder.addAll("e", "f"), is(sameInstance(builder)));
        assertThat(builder.addAll(Iterables.unmodifiableIterable(UnmodifiableList.listOf("g"))),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf("b", "d").iterator()),
                   is(sameInstance(builder)));
        assertThat(builder.addAll(UnmodifiableList.listOf()), is(sameInstance(builder)));
        assertThat(builder.addAll(Collections.enumeration(UnmodifiableList.listOf("i"))),
                   is(sameInstance(builder)));
        assertThat(builder.build(),
                   contains("a", "c", "b", "e", "f", "g", "b", "d", "i"));
    }

    @Test
    public void testSubList() {
        List<String> test = UnmodifiableList.listOf("a", "b", "c");
        assertThat(test.subList(0, 3), is(sameInstance(test)));
        assertThat(test.subList(1, 1), is(sameInstance(UnmodifiableList.listOf())));
        assertThat(test.subList(0, 1), contains("a"));
        assertThat(test.subList(2, 3), contains("c"));
    }

    @Test
    public void testCollection_Methods() {
        assertThat(UnmodifiableList.listOf().contains("foo"), is(false));
        assertThat(UnmodifiableList.listOf("foo").contains("foo"), is(true));
        assertThat(UnmodifiableList.listOf("foo").contains("bar"), is(false));
        assertThat(UnmodifiableList.listOf("foo").containsAll(UnmodifiableList.listOf("foo", "bar")), is(false));
        assertThat(UnmodifiableList.listOf("foo", "bar").containsAll(UnmodifiableList.listOf("foo", "bar")), is(true));
    }

    @Test
    public void testCollection_ListIterator() {
        ListIterator<String> it = UnmodifiableList.listOf("a", "b", "c").listIterator(2);
        assertThat(it.hasPrevious(), is(true));
        assertThat(it.previousIndex(), is(1));
        assertThat(it.nextIndex(), is(2));
        assertThat(it.next(), is("c"));
        assertThat(it.hasNext(), is(false));
        try {
            it.next();
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Index 3 out of bounds for length 3"));
        }
        assertThat(it.previous(), is("c"));
        assertThat(it.previous(), is("b"));
        assertThat(it.previous(), is("a"));
        assertThat(it.hasPrevious(), is(false));
        try {
            it.previous();
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Index -1 out of bounds for length 3"));
        }
    }

    @Test
    public void testBadInput() {
        try {
            UnmodifiableList.listOf("foo").get(1);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), anyOf(
                    is("Index 1 out of bounds for length 1"),
                    // Case for java 8
                    is("1")));
        }
        try {
            UnmodifiableList.listOf("foo").listIterator(2);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("ListIterator index 2 out of bounds for length 1"));
        }
        try {
            UnmodifiableList.listOf("foo", "bar", "baz").subList(-1, 2);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("Start index -1 out of bounds for length 3"));
        }
        try {
            UnmodifiableList.listOf("foo", "bar", "baz").subList(2, 1);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("End index 1 out of bounds for length 3, start 2"));
        }
        try {
            UnmodifiableList.listOf("foo", "bar", "baz").subList(1, 5);
            fail("no exception");
        } catch (IndexOutOfBoundsException e) {
            assertThat(e.getMessage(), is("End index 5 out of bounds for length 3, start 1"));
        }
    }

    @Test
    public void testUnsupported_Collection() {
        try {
            UnmodifiableList.listOf().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().remove(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().addAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().removeAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().removeIf(Predicate.isEqual(new Object()));
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().retainAll(new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().clear();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }

    @Test
    public void testUnsupported_List() {
        try {
            UnmodifiableList.listOf().add(0, new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().remove(0);
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().addAll(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().set(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().add(0, new LinkedList<>());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().remove(0);
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }

    @Test
    public void testUnsupported_ListIterator() {
        try {
            UnmodifiableList.listOf().listIterator().remove();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().listIterator().set(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
        try {
            UnmodifiableList.listOf().listIterator().add(new Object());
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Operation not allowed"));
        }
    }
}
