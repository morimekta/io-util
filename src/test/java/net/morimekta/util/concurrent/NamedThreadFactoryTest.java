package net.morimekta.util.concurrent;

import org.junit.Test;

import java.util.concurrent.ThreadFactory;

import static java.lang.Thread.NORM_PRIORITY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NamedThreadFactoryTest {
    @Test
    public void testCreate() {
        ThreadFactory factory = NamedThreadFactory.builder()
                                                  .build();
        Thread thread = factory.newThread(() -> {});
        assertThat(thread.getName(), is("thread-1"));
        assertThat(thread.getPriority(), is(NORM_PRIORITY));
        assertThat(thread.isDaemon(), is(false));

        thread = factory.newThread(() -> {});
        assertThat(thread.getName(), is("thread-2"));
        assertThat(thread.getPriority(), is(NORM_PRIORITY));
        assertThat(thread.isDaemon(), is(false));

        factory = NamedThreadFactory.builder()
                                    .setNameFormat("foo")
                                    .setDaemon(true)
                                    .setPriority(7)
                                    .build();

        thread = factory.newThread(() -> {});
        assertThat(thread.getName(), is("foo"));
        assertThat(thread.isDaemon(), is(true));
        assertThat(thread.getPriority(), is(7));
    }
}
