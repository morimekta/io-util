package net.morimekta.util;

import net.morimekta.util.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TemporaryAssetFolderTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testTempAssetFolder() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(tmp.getRoot().toPath())) {
            tempDir = taf.getPath();

            assertThat(taf.getFile(), is(taf.getPath().toFile()));
            assertThat(taf.resolvePath("foo").toFile(), is(taf.resolveFile("foo")));

            Files.write(taf.getPath().resolve("foo1"), "bar1".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo2"), "bar2".getBytes(UTF_8));
            Files.createSymbolicLink(taf.getPath().resolve("foo3"), Paths.get("foo2"));
            Files.createDirectories(taf.getPath().resolve("foo4"));
            Files.write(taf.getPath().resolve("foo4/foo"), "bar4".getBytes(UTF_8));

            long num = FileUtil.list(tmp.getRoot().toPath()).size();
            assertThat(num, is(1L));

            num = FileUtil.list(taf.getPath()).size();
            assertThat(num, is(4L));
        }

        long num = FileUtil.list(tmp.getRoot().toPath()).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_File() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(tmp.getRoot())) {
            tempDir = taf.getPath();
            Files.write(taf.getPath().resolve("foo1"), "bar1".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo2"), "bar2".getBytes(UTF_8));
            Files.createSymbolicLink(taf.getPath().resolve("foo3"), Paths.get("foo2"));
            Files.createDirectories(taf.getPath().resolve("foo4"));
            Files.write(taf.getPath().resolve("foo4/foo"), "bar4".getBytes(UTF_8));
            Files.write(taf.getPath().resolve("foo4/bar"), "bar5".getBytes(UTF_8));

            long num = FileUtil.list(tmp.getRoot().toPath()).size();
            assertThat(num, is(1L));

            num = FileUtil.list(taf.getPath()).size();
            assertThat(num, is(4L));

            assertThat(taf.list().size(), is(4));
            assertThat(taf.list(true).size(), is(5));
        }

        long num = FileUtil.list(tmp.getRoot().toPath()).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_Utils() throws IOException {
        Path tempDir;
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(tmp.getRoot())) {
            tempDir = taf.getPath();

            taf.put("foo1", "bar1");
            taf.put("foo2/bar", Binary.wrap("bar2".getBytes(UTF_8)));
            taf.put("foo3", "bar3".getBytes(UTF_8));
            try (OutputStream out = taf.getOutputStream("foo4")) {
                out.write("bar4".getBytes(UTF_8));
            }
            try (OutputStream out = taf.getOutputStream("foo4", true)) {
                out.write(",bar4".getBytes(UTF_8));
            }
            try (Writer writer = taf.getWriter("foo5")) {
                writer.write("bar5");
            }
            try (Writer writer = taf.getWriter("foo5", true)) {
                writer.write(",bar5");
            }
            try (StringReader reader = new StringReader("bar6")) {
                taf.write("foo6", reader);
            }

            assertThat(taf.getString("foo1"), is("bar1"));
            assertThat(taf.getBinary("foo2/bar"), is(Binary.wrap("bar2".getBytes(UTF_8))));
            assertThat(taf.getBytes("foo3"), is("bar3".getBytes(UTF_8)));
            try (InputStream in = taf.getInputStream("foo4")) {
                assertThat(IOUtils.readString(in), is("bar4,bar4"));
            }
            try (Reader reader = taf.getReader("foo5")) {
                assertThat(IOUtils.readString(reader), is("bar5,bar5"));
            }
            assertThat(taf.getString("foo6"), is("bar6"));
        }

        long num = FileUtil.list(tmp.getRoot().toPath()).size();
        assertThat(num, is(0L));
        assertThat(Files.exists(tempDir), is(false));
    }

    @Test
    public void testTempAssetFolder_BadResolve() throws IOException {
        try (TemporaryAssetFolder taf = new TemporaryAssetFolder(tmp.getRoot())) {
            try {
                taf.resolvePath(".." + File.separator);
                fail("No exception");
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Up path not allowed in file name"));
            }
        }
    }
}
