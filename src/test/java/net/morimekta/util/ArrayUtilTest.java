package net.morimekta.util;

import org.junit.Test;

import static net.morimekta.util.ArrayUtil.booleanPrimitives;
import static net.morimekta.util.ArrayUtil.bytePrimitives;
import static net.morimekta.util.ArrayUtil.doublePrimitives;
import static net.morimekta.util.ArrayUtil.floatPrimitives;
import static net.morimekta.util.ArrayUtil.intPrimitives;
import static net.morimekta.util.ArrayUtil.listPrimitives;
import static net.morimekta.util.ArrayUtil.longPrimitives;
import static net.morimekta.util.ArrayUtil.shortPrimitives;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.Unmodifiables.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

public class ArrayUtilTest {
    @Test
    public void testPrimitives() {
        assertThat(booleanPrimitives(true, false, false),
                   is(new boolean[]{true, false, false}));
        assertThat(bytePrimitives(new Byte[]{3, 4, 5}),
                   is(new byte[]{3, 4, 5}));
        assertThat(shortPrimitives(new Short[]{3, 4, 5}),
                   is(new short[]{3, 4, 5}));
        assertThat(intPrimitives(3, 4, 5),
                   is(new int[]{3, 4, 5}));
        assertThat(longPrimitives(3L, 4L, 5L),
                   is(new long[]{3, 4, 5}));
        assertThat(floatPrimitives(1.1F, 2.2F, 3.3F),
                   is(new float[]{1.1F, 2.2F, 3.3F}));
        assertThat(doublePrimitives(1.1D, 2.2D, 3.3D),
                   is(new double[]{1.1, 2.2, 3.3}));

        assertThat(booleanPrimitives(asList(true, false, false)),
                   is(new boolean[]{true, false, false}));
        assertThat(bytePrimitives(asList(new Byte[]{3, 4, 5})),
                   is(new byte[]{3, 4, 5}));
        assertThat(shortPrimitives(asList(new Short[]{3, 4, 5})),
                   is(new short[]{3, 4, 5}));
        assertThat(intPrimitives(asList(3, 4, 5)),
                   is(new int[]{3, 4, 5}));
        assertThat(longPrimitives(asList(3L, 4L, 5L)),
                   is(new long[]{3, 4, 5}));
        assertThat(floatPrimitives(asList(1.1F, 2.2F, 3.3F)),
                   is(new float[]{1.1F, 2.2F, 3.3F}));
        assertThat(doublePrimitives(asList(1.1D, 2.2D, 3.3D)),
                   is(new double[]{1.1, 2.2, 3.3}));

        assertThat(booleanPrimitives(), is(sameInstance(booleanPrimitives(asList()))));
        assertThat(bytePrimitives(), is(sameInstance(bytePrimitives(asList()))));
        assertThat(shortPrimitives(), is(sameInstance(shortPrimitives(asList()))));
        assertThat(intPrimitives(), is(sameInstance(intPrimitives(asList()))));
        assertThat(longPrimitives(), is(sameInstance(longPrimitives(asList()))));
        assertThat(floatPrimitives(), is(sameInstance(floatPrimitives(asList()))));
        assertThat(doublePrimitives(), is(sameInstance(doublePrimitives(asList()))));

        assertThat(listPrimitives(new boolean[]{true, false}),
                   is(listOf(true, false)));
        assertThat(listPrimitives(new byte[]{3, 5}),
                   is(listOf((byte)3, (byte) 5)));
        assertThat(listPrimitives(new short[]{2, 4, 6}),
                   is(listOf((short) 2, (short) 4, (short) 6)));
        assertThat(listPrimitives(new int[]{4, 3, 2}),
                   is(listOf(4, 3, 2)));
        assertThat(listPrimitives(new long[]{2, 3, 4}),
                   is(listOf(2L, 3L, 4L)));
        assertThat(listPrimitives(new float[]{1.1f, 2.2f}),
                   is(listOf(1.1f, 2.2f)));
        assertThat(listPrimitives(new double[]{1.1, 2.2}),
                   is(listOf(1.1, 2.2)));

        assertThat(listPrimitives(new boolean[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new byte[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new short[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new int[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new long[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new float[]{}),
                   is(sameInstance(listOf())));
        assertThat(listPrimitives(new double[]{}),
                   is(sameInstance(listOf())));
    }
}
