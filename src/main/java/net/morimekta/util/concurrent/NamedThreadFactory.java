package net.morimekta.util.concurrent;

import javax.annotation.Nonnull;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.NORM_PRIORITY;

public class NamedThreadFactory implements ThreadFactory {
    private final AtomicInteger nextId;
    private final String nameFormat;
    private final boolean isDaemon;
    private final int priority;

    private NamedThreadFactory(Builder builder) {
        this.nextId = new AtomicInteger();
        this.nameFormat = builder.nameFormat;
        this.isDaemon = builder.isDaemon;
        this.priority = builder.priority;
    }

    @Nonnull
    @Override
    public Thread newThread(@Nonnull Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setName(String.format(nameFormat, nextId.incrementAndGet()));
        thread.setDaemon(isDaemon);
        thread.setPriority(priority);
        return thread;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String  nameFormat;
        private boolean isDaemon;
        private int     priority;

        Builder() {
            nameFormat = "thread-%d";
            isDaemon = false;
            priority = NORM_PRIORITY;
        }

        public NamedThreadFactory build() {
            return new NamedThreadFactory(this);
        }

        public Builder setNameFormat(String nameFormat) {
            this.nameFormat = nameFormat;
            return this;
        }

        public Builder setDaemon(boolean daemon) {
            isDaemon = daemon;
            return this;
        }

        public Builder setPriority(int priority) {
            this.priority = priority;
            return this;
        }
    }
}
