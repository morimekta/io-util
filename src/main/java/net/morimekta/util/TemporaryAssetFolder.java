package net.morimekta.util;

import net.morimekta.util.io.IOUtils;

import javax.annotation.Nonnull;
import javax.annotation.WillNotClose;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * A temporary asset folder where an arbitrary number of files can be added
 * to and should guarantee complete cleanup on close. Example usage:
 *
 * <pre>{@code
 * try (TemporaryAssetFolder tmp = new TemporaryAssetFolder(base)) {
 *     // do stuff
 * }
 * // is all deleted.
 * }</pre>
 *
 * This will create a temp folder into the base directory for each run, where
 * any number of files or sub-folders can be added. All of it will be removed
 * immediately on close.
 */
public class TemporaryAssetFolder implements Closeable {
    private static final String PARENT_DIR = "..";

    private final Path path;

    public TemporaryAssetFolder(@Nonnull File baseTempDirectory) throws IOException {
        this(baseTempDirectory.toPath());
    }

    public TemporaryAssetFolder(@Nonnull Path baseTempDirectory) throws IOException {
        path = Files.createTempDirectory(baseTempDirectory, "tmp");
    }

    public Path getPath() {
        return path;
    }

    public File getFile() {
        return getPath().toFile();
    }

    public Path resolvePath(String name) {
        if (name.contains(File.separator + PARENT_DIR + File.separator) || name.startsWith(PARENT_DIR + File.separator)) {
            throw new IllegalArgumentException("Up path not allowed in file name");
        }
        return getPath().resolve(name);
    }

    public File resolveFile(String name) {
        return resolvePath(name).toFile();
    }

    public Path put(String name, byte[] content) throws IOException {
        return put(name, Binary.wrap(content));
    }

    public Path put(String name, Binary content) throws IOException {
        return write(name, content.getInputStream());
    }

    public Path write(String name, InputStream contentStream) throws IOException {
        Path file = resolvePath(name);
        Files.createDirectories(file.getParent());
        try (OutputStream out = new FileOutputStream(file.toFile())) {
            IOUtils.copy(contentStream, out);
            out.flush();
        }
        return file;
    }

    public Path put(String name, CharSequence content) throws IOException {
        Path file = resolvePath(name);
        Files.createDirectories(file.getParent());
        try (OutputStream out = new FileOutputStream(file.toFile());
             Writer writer = new OutputStreamWriter(out, UTF_8)) {
            writer.write(content.toString());
            out.flush();
        }
        return file;
    }

    public Path write(String name, Reader reader) throws IOException {
        return put(name, IOUtils.readString(reader));
    }

    public String getString(String name) throws IOException {
        try (InputStream in = new FileInputStream(resolvePath(name).toFile())) {
            return IOUtils.readString(in);
        }
    }

    public byte[] getBytes(String name) throws IOException {
        try (InputStream in = new FileInputStream(resolvePath(name).toFile())) {
            ByteArrayOutputStream tmp = new ByteArrayOutputStream();
            IOUtils.copy(in, tmp);
            return tmp.toByteArray();
        }
    }

    public Binary getBinary(String name) throws IOException {
        try (InputStream in = new FileInputStream(resolvePath(name).toFile())) {
            return Binary.read(in);
        }
    }

    @WillNotClose
    public InputStream getInputStream(String name) throws FileNotFoundException {
        return new FileInputStream(resolvePath(name).toFile());
    }

    @WillNotClose
    public Reader getReader(String name) throws FileNotFoundException {
        return new InputStreamReader(getInputStream(name), UTF_8);
    }

    @WillNotClose
    public OutputStream getOutputStream(String name) throws FileNotFoundException {
        return getOutputStream(name, false);
    }

    @WillNotClose
    public OutputStream getOutputStream(String name, boolean append) throws FileNotFoundException {
        return new FileOutputStream(resolvePath(name).toFile(), append);
    }

    @WillNotClose
    public Writer getWriter(String name) throws FileNotFoundException {
        return getWriter(name, false);
    }

    @WillNotClose
    public Writer getWriter(String name, boolean append) throws FileNotFoundException {
        return new OutputStreamWriter(getOutputStream(name, append), UTF_8);
    }

    public Collection<Path> list() throws IOException {
        return FileUtil.list(path);
    }

    public Collection<Path> list(boolean recursive) throws IOException {
        return FileUtil.list(path, recursive);
    }

    @Override
    public void close() throws IOException {
        FileUtil.deleteRecursively(path);
    }
}
