package net.morimekta.util.collect;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableMapCollector<E, K, V> implements Collector<E, UnmodifiableMap.Builder<K, V>, UnmodifiableMap<K, V>> {
    private final Function<E, K> toKey;
    private final Function<E, V> toValue;

    public UnmodifiableMapCollector(Function<E, K> toKey, Function<E, V> toValue) {
        this.toKey = toKey;
        this.toValue = toValue;
    }

    @Override
    public Supplier<UnmodifiableMap.Builder<K, V>> supplier() {
        return UnmodifiableMap::builder;
    }

    @Override
    public BiConsumer<UnmodifiableMap.Builder<K, V>, E> accumulator() {
        return (b, e) -> b.put(toKey.apply(e), toValue.apply(e));
    }

    @Override
    public BinaryOperator<UnmodifiableMap.Builder<K, V>> combiner() {
        return (b1, b2) -> b1.putAll(b2.build());
    }

    @Override
    public Function<UnmodifiableMap.Builder<K, V>, UnmodifiableMap<K, V>> finisher() {
        return UnmodifiableMap.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf();
    }
}
