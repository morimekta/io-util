package net.morimekta.util.collect;

import java.util.Comparator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableSortedSetCollector<E> implements Collector<E, UnmodifiableSortedSet.Builder<E>, UnmodifiableSortedSet<E>> {
    private final Comparator<E> comparator;

    UnmodifiableSortedSetCollector(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public Supplier<UnmodifiableSortedSet.Builder<E>> supplier() {
        return () -> UnmodifiableSortedSet.builderOrderedBy(comparator);
    }

    @Override
    public BiConsumer<UnmodifiableSortedSet.Builder<E>, E> accumulator() {
        return UnmodifiableSortedSet.Builder::add;
    }

    @Override
    public BinaryOperator<UnmodifiableSortedSet.Builder<E>> combiner() {
        return (b1, b2) -> b1.addAll(b2.build());
    }

    @Override
    public Function<UnmodifiableSortedSet.Builder<E>, UnmodifiableSortedSet<E>> finisher() {
        return UnmodifiableSortedSet.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf(Characteristics.UNORDERED);
    }
}