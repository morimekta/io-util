package net.morimekta.util.collect;

import net.morimekta.util.Pair;

import javax.annotation.Nonnull;
import java.util.Comparator;
import java.util.Set;
import java.util.SortedSet;

import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static net.morimekta.util.collect.UnmodifiableSortedSet.builderOrderedBy;

/**
 * Utility class containing set-math operations.
 * <p>
 * See <a href="https://www.probabilitycourse.com/chapter1/1_2_2_set_operations.php">probabilitycouse</a>
 * or <a href="https://www.britannica.com/science/set-theory/Operations-on-sets">britannica</a>
 * for definitions and theory.
 */
public class SetOperations {
    /**
     * Get the union of all the set's provided. Essentially a set
     * of all elements in each set in the input.
     *
     * @param of Sets to make union of.
     * @param <E> The item type.
     * @return The union set.
     */
    @Nonnull
    @SafeVarargs
    public static <E> Set<E> union(@Nonnull Set<E>... of) {
        if (of.length == 0) return setOf();
        if (of[0] instanceof SortedSet) {
            SortedSet<E> first = (SortedSet<E>) of[0];
            @SuppressWarnings("unchecked")
            UnmodifiableSortedSet.Builder<E> builder = builderOrderedBy((Comparator) first.comparator());
            for (Set<E> set : of) {
                builder.addAll(set);
            }
            return builder.build();
        } else {
            UnmodifiableSet.Builder<E> builder = UnmodifiableSet.builder();
            for (Set<E> set : of) {
                builder.addAll(set);
            }
            return builder.build();
        }
    }

    /**
     * Get the intersection between the sets. These are the items contains
     * in all of the sets provided.
     *
     * @param set The main set to make intersection with.
     * @param with Sets to intersect with.
     * @param <E> The item type.
     * @return The intersected set.
     */
    @Nonnull
    @SafeVarargs
    public static <E> Set<E> intersect(@Nonnull Set<E> set, @Nonnull Set<E>... with) {
        if (with.length == 0) {
            if (set instanceof SortedSet) {
                return UnmodifiableSortedSet.copyOf(set);
            }
            return UnmodifiableSet.copyOf(set);
        }
        if (set instanceof SortedSet) {
            SortedSet<E> first = (SortedSet<E>) set;
            @SuppressWarnings("unchecked")
            UnmodifiableSortedSet.Builder<E> builder = builderOrderedBy((Comparator) first.comparator());
            setLoop: for (E e : set) {
                for (Set<E> w : with) {
                    if (!w.contains(e)) continue setLoop;
                }
                builder.add(e);
            }
            return builder.build();
        } else {
            UnmodifiableSet.Builder<E> builder = UnmodifiableSet.builder();
            setLoop: for (E e : set) {
                for (Set<E> w : with) {
                    if (!w.contains(e)) continue setLoop;
                }
                builder.add(e);
            }
            return builder.build();
        }
    }

    /**
     * Take the minuend, and subtract all elements contained in all the subtrahend sets.
     *
     * @param minuend The source set to be removed elements from.
     * @param subtrahends The sets with elements to remove.
     * @param <E> The element type.
     * @return The difference set from the subtraction.
     */
    @Nonnull
    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static <E> Set<E> subtract(@Nonnull Set<E> minuend, @Nonnull Set<E>... subtrahends) {
        if (subtrahends.length == 0) {
            if (minuend instanceof SortedSet) {
                return UnmodifiableSortedSet.copyOf(minuend);
            }
            return UnmodifiableSet.copyOf(minuend);
        }
        UnmodifiableCollectionBuilder<E, ? extends Set, ?> builder;
        if (minuend instanceof SortedSet) {
            SortedSet<E> first = (SortedSet<E>) minuend;
            builder = builderOrderedBy((Comparator) first.comparator());
        } else {
            builder = UnmodifiableSet.builder();
        }

        setLoop: for (E e : minuend) {
            for (Set<E> w : subtrahends) {
                if (w.contains(e)) continue setLoop;
            }
            builder.add(e);
        }
        return builder.build();
    }

    /**
     * Create a difference set of the items contained only in either
     * of the two sets, but not both. This is the same as
     * <code>union(subtract(a, b), subtract(b, a))</code>, but much more
     * efficient.
     *
     * @param first The first set.
     * @param second The second set.
     * @param <E> The item type.
     * @return The difference set.
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> Set<E> difference(@Nonnull Set<E> first, @Nonnull Set<E> second) {
        UnmodifiableCollectionBuilder<E, ? extends Set, ?> builder;
        if (first instanceof SortedSet) {
            if (first.isEmpty()) {
                return UnmodifiableSortedSet.copyOf(second);
            }
            if (second.isEmpty()) {
                return UnmodifiableSortedSet.copyOf(first);
            }
            builder = UnmodifiableSortedSet.builderOrderedBy(
                    first.size() + second.size(),
                    (Comparator<E>) ((SortedSet<E>) first).comparator());
        } else {
            if (first.isEmpty()) {
                return UnmodifiableSet.copyOf(second);
            }
            if (second.isEmpty()) {
                return UnmodifiableSet.copyOf(first);
            }
            builder = UnmodifiableSet.builder(first.size() + second.size());
        }

        for (E e : first) {
            if (!second.contains(e)) {
                builder.add(e);
            }
        }
        for (E e : second) {
            if (!first.contains(e)) {
                builder.add(e);
            }
        }
        return builder.build();
    }

    /**
     * Calculate the cartesian product of the two sets. The cartesian product will
     * combine every element of A with every element of B, so the resulting set
     * will have a total of <code>size(A) * size(B)</code> elements each with a
     * unique combination of <code>(Ax, By)</code> pair.
     *
     * @param a The first set
     * @param b The second set
     * @param <A> The element type in first set
     * @param <B> The element type in second set
     * @return The cartesian product.
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    public static <A, B> Set<Pair<A, B>> product(@Nonnull Set<A> a, @Nonnull Set<B> b) {
        UnmodifiableCollectionBuilder<Pair<A, B>, ? extends Set, ?> builder;
        if (a instanceof SortedSet && b instanceof SortedSet) {
            SortedSet<A> sa = (SortedSet<A>) a;
            SortedSet<B> sb = (SortedSet<B>) b;

            Comparator<Pair<A, B>> comparator = sa.comparator() == null ?
                                                Comparator.comparing(p -> (Comparable) p.first) :
                                                Comparator.comparing(p -> p.first, sa.comparator());
            if (sb.comparator() == null) {
                comparator = comparator.thenComparing(p -> (Comparable) p.second);
            } else {
                comparator = comparator.thenComparing(p -> p.second, sb.comparator());
            }

            if (a.isEmpty() || b.isEmpty()) {
                return UnmodifiableSortedSet.builderOrderedBy(comparator).build();
            }

            builder = UnmodifiableSortedSet.builderOrderedBy(a.size() * b.size(), comparator);
        } else {
            if (a.isEmpty() || b.isEmpty()) {
                return UnmodifiableSet.setOf();
            }
            builder = UnmodifiableSet.builder(a.size() * b.size());
        }

        for (A ia : a) {
            for (B ib : b) {
                builder.add(Pair.create(ia, ib));
            }
        }
        return builder.build();
    }

    /**
     * Check if the first set is a subset of the second.
     *
     * @param set The set to check.
     * @param of The set to check against.
     * @param <E> The item type.
     * @return True if the first set is a subset of the second.
     */
    public static <E> boolean subset(@Nonnull Set<E> set, @Nonnull Set<E> of) {
        if (set.size() > of.size()) return false;
        for (E e : set) {
            if (!of.contains(e)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check that the two sets are disjoint, meaning they have no items in common.
     * Same that the intersection between the sets are empty.
     *
     * @param first The first set.
     * @param second The second set.
     * @param <E> The item type.
     * @return True if the two sets are disjoint.
     */
    public static <E> boolean disjoint(@Nonnull Set<E> first, @Nonnull Set<E> second) {
        // We actually only need to check one way, as the equality
        // items always comes in pairs...
        for (E item : first) {
            if (second.contains(item)) {
                return false;
            }
        }
        return true;
    }

    private SetOperations() {}
}
