/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;

import static java.lang.Math.max;

@Immutable
public final class UnmodifiableSortedSet<E> extends UnmodifiableCollection<E> implements SortedSet<E> {
    // -------- Constructor --------

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> copyOf(@Nonnull E[] array) {
        if (array.length == 0) return sortedSetOf();
        Object[] copy = Arrays.copyOf(array, array.length);
        Arrays.sort(copy);
        return new UnmodifiableSortedSet<>(0, copy.length, copy, null);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSortedSet<E> copyOf(@Nonnull Iterable<? extends E> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection<E>) iterable);
        }
        return copyOf(iterable.iterator());
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> copyOf(@Nonnull Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) return sortedSetOf();
        return UnmodifiableSortedSet.<E>builderOrderedBy(null).addAll(iterator).build();
    }

    @Nonnull
    public static <E extends Comparable<E>> UnmodifiableSortedSet<E> copyOf(@Nonnull Enumeration<? extends E> enumeration) {
        if (!enumeration.hasMoreElements()) return sortedSetOf();
        return UnmodifiableSortedSet.<E>builderNaturalOrder().addAll(enumeration).build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSortedSet<E> copyOf(@Nonnull Collection<? extends E> collection) {
        if (collection instanceof UnmodifiableSortedSet) {
            return (UnmodifiableSortedSet<E>) collection;
        }
        if (collection instanceof UnmodifiableSet) {
            Object[] array = collection.toArray();
            Arrays.sort(array);
            return new UnmodifiableSortedSet<>(0, array.length, array, null);
        }
        Comparator comparator = null;
        if (collection instanceof SortedSet) {
            comparator = ((SortedSet<? extends E>) collection).comparator();
        }
        if (collection.isEmpty()) return builderOrderedBy(comparator).build();
        return UnmodifiableSortedSet.<E>builderOrderedBy(collection.size(), comparator)
                .addAll(collection)
                .build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSortedSet<E> sortedSetOf() {
        return (UnmodifiableSortedSet<E>) EMPTY;
    }


    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e) {
        return construct(e);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2) {
        return construct(e1, e2);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3) {
        return construct(e1, e2, e3);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4) {
        return construct(e1, e2, e3, e4);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5) {
        return construct(e1, e2, e3, e4, e5);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5, E e6) {
        return construct(e1, e2, e3, e4, e5, e6);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7) {
        return construct(e1, e2, e3, e4, e5, e6, e7);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8);
    }

    @Nonnull
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9);
    }

    @Nonnull
    @SafeVarargs
    public static <E> UnmodifiableSortedSet<E> sortedSetOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E... more) {
        Object[] array = new Object[10 + more.length];
        array[0] = e1;
        array[1] = e2;
        array[2] = e3;
        array[3] = e4;
        array[4] = e5;
        array[5] = e6;
        array[6] = e7;
        array[7] = e8;
        array[8] = e9;
        array[9] = e10;
        System.arraycopy(more, 0, array, 10, more.length);
        return construct(array);
    }

    public static <E> Builder<E> builderOrderedBy(Comparator<E> comparator) {
        return new Builder<>(4, comparator);
    }
    public static <E> Builder<E> builderOrderedBy(int initialCapacity, Comparator<E> comparator) {
        return new Builder<>(initialCapacity, comparator);
    }

    public static <E extends Comparable<E>> Builder<E> builderNaturalOrder() {
        return new Builder<>(4, null);
    }
    public static <E extends Comparable<E>> Builder<E> builderNaturalOrder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity), null);
    }
    public static <E extends Comparable<E>> Builder<E> builderReverseOrder() {
        return new Builder<>(4, Comparator.reverseOrder());
    }
    public static <E extends Comparable<E>> Builder<E> builderReverseOrder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity), Comparator.reverseOrder());
    }

    // -------- Object --------

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Set)) return false;
        Set other = (Set) obj;
        if (other.size() != length) return false;
        for (Object o : (Set) obj) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        for (int i = offset; i < (offset + length); ++i) {
            if (i > 0) builder.append(", ");
            builder.append(array[i].toString());
        }
        return builder.append("}").toString();
    }

    @Override
    public int hashCode() {
        if (hashCode == null) {
            int hash = Objects.hash(getClass(), length);
            for (int i = offset; i < (offset + length); ++i) {
                hash ^= Objects.hash(array[i]);
            }
            hashCode = hash;
        }
        return hashCode;
    }

    // -------- Collection --------

    @Override
    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
        if (length == 0) return false;
        return Arrays.binarySearch(array, offset, offset + length, o, comparator) >= 0;
    }

    // -------- SortedSet --------

    @Override
    @SuppressWarnings("unchecked")
    public Comparator<E> comparator() {
        return comparator;
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public UnmodifiableSortedSet<E> subSet(@Nonnull E start, @Nonnull E end) {
        int si = Arrays.binarySearch(array, offset, offset + length, start, comparator);
        if (si < 0) {
            si = -1 - si;
        }
        int ei = Arrays.binarySearch(array, offset, offset + length, end, comparator);
        if (ei < 0) {
            ei = -1 - ei;
        }
        if (si >= ei) return sortedSetOf();
        if (si == offset && ei == (offset + length)) return this;
        int len = ei - si;
        return new UnmodifiableSortedSet<>(si, len, array, comparator);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public UnmodifiableSortedSet<E> headSet(@Nonnull E end) {
        int ei = Arrays.binarySearch(array, offset, offset + length, end, comparator);
        if (ei < 0) {
            ei = -1 - ei;
        }
        if (ei == offset) return sortedSetOf();
        if (ei == offset + length) return this;
        int len = ei - offset;
        return new UnmodifiableSortedSet<>(offset, len, array, comparator);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public UnmodifiableSortedSet<E> tailSet(@Nonnull E start) {
        int si = Arrays.binarySearch(array, offset, offset + length, start, comparator);
        if (si < 0) {
            si = -1 - si;
        }
        if (si == offset + length) return sortedSetOf();
        if (si == offset) return this;
        int len = (offset + length) - si;
        return new UnmodifiableSortedSet<>(si, len, array, comparator);
    }

    @Override
    @SuppressWarnings("unchecked")
    public E first() {
        return length > 0 ? (E) array[offset] : null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E last() {
        return length > 0 ? (E) array[offset + length - 1] : null;
    }

    // -------- UnmodifiableSortedSet --------

    public final static class Builder<E> implements UnmodifiableCollectionBuilder<E, UnmodifiableSortedSet<E>, Builder<E>> {
        private Object[]              array;
        private int                   size;
        private UnmodifiableSortedSet justBuilt;

        private final Comparator comparator;

        Builder(int initialCapacity, Comparator comparator) {
            this.array = new Object[initialCapacity];
            this.size = 0;
            this.justBuilt = null;
            this.comparator = comparator;
        }

        @Nonnull
        @Override
        public Builder<E> add(@Nonnull E e) {
            checkNotNull(e);
            ensureCapacity(size + 1);
            array[size++] = e;
            return this;
        }

        @Nonnull
        @Override
        @SafeVarargs
        public final Builder<E> addAll(E... items) {
            if (items.length == 0) {
                return this;
            }
            checkNotNull(items);
            ensureCapacity(size + items.length);
            System.arraycopy(items, 0, array, size, items.length);
            size += items.length;
            return this;
        }

        @Nonnull
        @Override
        public Builder<E> addAll(@Nonnull Collection<? extends E> collection) {
            if (collection.isEmpty()) {
                return this;
            }
            checkNotNull(collection);
            ensureCapacity(size + collection.size());
            for (Object o : collection) {
                array[size++] = o;
            }
            return this;
        }

        @Override
        @SuppressWarnings("unchecked")
        public UnmodifiableSortedSet<E> build() {
            if (comparator == null && size == 0) return sortedSetOf();
            if (justBuilt != null) return justBuilt;
            makeSorted(1);
            justBuilt = new UnmodifiableSortedSet(0, size, array, comparator);
            return justBuilt;
        }

        @SuppressWarnings("unchecked")
        private void makeSorted(int minSize) {
            if (size == 0) return;

            // A: Sort
            Arrays.sort(array, 0, size, comparator);

            // B: Deduplicate
            final int originalSize = size;
            for (int i = 1; i < originalSize; ++i) {
                if (array[i].equals(array[i - 1])) {
                    // The last entry is kept.
                    array[i - 1] = null;
                    --size;
                }
            }

            // C: Compact away null values.
            if (size < originalSize) {
                if (originalSize - max(size, minSize) > 1024) {
                    // In order to save memory, minimize larger arrays.
                    Object[] compact = new Object[max(minSize, size)];
                    int pos = 0;
                    for (int i = 0; i < originalSize; ++i) {
                        Object o = array[i];
                        if (o != null) {
                            compact[pos++] = o;
                        }
                    }
                    array = compact;
                } else {
                    for (int moveTo = 0, moveFrom = 1;
                         moveTo < size && moveFrom < originalSize;
                         ++moveTo, ++moveFrom) {
                        if (array[moveTo] == null) {
                            while (array[moveFrom] == null) ++moveFrom;
                            array[moveTo] = array[moveFrom];
                            array[moveFrom] = null;
                        }
                    }
                }
            }
        }

        private void ensureCapacity(int newSize) {
            if (array.length < newSize) {
                Object[] old = array;
                array = new Object[old.length * 2];
                System.arraycopy(old, 0, array, 0, size);
                makeSorted(newSize);
            } else if (justBuilt != null) {
                Object[] old = array;
                array = new Object[old.length];
                System.arraycopy(old, 0, array, 0, size);
                // Already sorted.
            }
            justBuilt = null;
        }
    }

    // -------- Private --------

    private static final UnmodifiableSortedSet<Object> EMPTY = new UnmodifiableSortedSet<>(0, 0, EMPTY_ARRAY, Comparator.naturalOrder());

    @Nullable
    private final transient Comparator comparator;

    UnmodifiableSortedSet(int offset, int length, @Nonnull Object[] array, @Nullable Comparator comparator) {
        super(offset, length, array);
        this.comparator = comparator;
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    private static <E> UnmodifiableSortedSet<E> construct(Object... array) {
        checkNotNull(array);

        // A: Sort
        Arrays.sort(array);

        // B: Deduplicate
        int size = array.length;
        for (int i = 1; i < array.length; ++i) {
            if (array[i].equals(array[i - 1])) {
                // The last entry is kept.
                array[i - 1] = null;
                --size;
            }
        }

        // C: Compact away null values.
        if (size < array.length) {
            if (array.length - size > 1024) {
                // In order to save memory, minimize larger arrays.
                Object[] compact = new Object[size];
                int pos = 0;
                for (Object o : array) {
                    if (o != null) {
                        compact[pos++] = o;
                    }
                }
                array = compact;
            } else {
                for (int moveTo = 0, moveFrom = 1;
                     moveTo < size && moveFrom < array.length;
                     ++moveTo, ++moveFrom) {
                    if (array[moveTo] == null) {
                        while (array[moveFrom] == null) ++moveFrom;
                        array[moveTo] = array[moveFrom];
                        array[moveFrom] = null;
                    }
                }
            }
        }

        return new UnmodifiableSortedSet<>(0, size, array, null);
    }
}
