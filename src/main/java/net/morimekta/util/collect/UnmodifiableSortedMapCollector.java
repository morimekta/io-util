package net.morimekta.util.collect;

import java.util.Comparator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableSortedMapCollector<E, K, V> implements Collector<E, UnmodifiableSortedMap.Builder<K, V>, UnmodifiableSortedMap<K, V>> {
    private final Function<E, K> toKey;
    private final Function<E, V> toValue;
    private final Comparator<K>  comparator;

    UnmodifiableSortedMapCollector(Function<E, K> toKey, Function<E, V> toValue, Comparator<K> comparator) {
        this.toKey = toKey;
        this.toValue = toValue;
        this.comparator = comparator;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Supplier<UnmodifiableSortedMap.Builder<K, V>> supplier() {
        return () -> (UnmodifiableSortedMap.Builder<K, V>) UnmodifiableSortedMap.builderOrderedBy(comparator);
    }

    @Override
    public BiConsumer<UnmodifiableSortedMap.Builder<K, V>, E> accumulator() {
        return (b, e) -> b.put(toKey.apply(e), toValue.apply(e));
    }

    @Override
    public BinaryOperator<UnmodifiableSortedMap.Builder<K, V>> combiner() {
        return (b1, b2) -> b1.putAll(b2.build());
    }

    @Override
    public Function<UnmodifiableSortedMap.Builder<K, V>, UnmodifiableSortedMap<K, V>> finisher() {
        return UnmodifiableSortedMap.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf(Characteristics.UNORDERED);
    }
}
