package net.morimekta.util.collect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableGroupingByCollector<E, K, V> implements Collector<E, Map<K, List<V>>, UnmodifiableMap<K, List<V>>> {
    private final Function<E, K> toKey;
    private final Function<E, V> toValue;

    public UnmodifiableGroupingByCollector(Function<E, K> toKey, Function<E, V> toValue) {
        this.toKey = toKey;
        this.toValue = toValue;
    }

    @Override
    public Supplier<Map<K, List<V>>> supplier() {
        return HashMap::new;
    }

    @Override
    public BiConsumer<Map<K, List<V>>, E> accumulator() {
        return (b, e) -> {
            b.computeIfAbsent(toKey.apply(e), k -> new ArrayList<>())
             .add(toValue.apply(e));
        };
    }

    @Override
    public BinaryOperator<Map<K, List<V>>> combiner() {
        return (b1, b2) -> {
            for (Map.Entry<K, List<V>> entry : b2.entrySet()) {
                b1.computeIfAbsent(entry.getKey(), k -> new ArrayList<>(entry.getValue().size()))
                  .addAll(entry.getValue());
            }
            return b1;
        };
    }

    @Override
    public Function<Map<K, List<V>>, UnmodifiableMap<K, List<V>>> finisher() {
        return map -> {
            UnmodifiableMap.Builder<K, List<V>> builder = UnmodifiableMap.builder();
            for (Map.Entry<K, List<V>> entry : map.entrySet()) {
                builder.put(entry.getKey(), UnmodifiableList.copyOf(entry.getValue()));
            }
            return builder.build();
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf();
    }
}
