/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static net.morimekta.util.collect.UnmodifiableCollection.checkNotNull;

public abstract class UnmodifiableMapBuilder<
        K, V,
        M extends UnmodifiableMapBase<K, V>,
        B extends UnmodifiableMapBuilder<K, V, M, B>> {
    @Nonnull
    public abstract B put(@Nonnull K key, @Nonnull V value);

    @Nonnull
    public abstract B putAll(@Nonnull Map<? extends K, ? extends V> map) ;

    public abstract M build();

    public B put(@Nonnull Map.Entry<K, V> entry) {
        return put(requireNonNull(entry.getKey()), requireNonNull(entry.getValue()));
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final B putAll(@Nonnull Map.Entry<K, V>... entries) {
        if (entries.length == 0) return (B) this;
        for (Map.Entry<K, V> entry : entries) {
            checkNotNull(entry);
            put(requireNonNull(entry.getKey()), requireNonNull(entry.getValue()));
        }
        return (B) this;
    }

    @SuppressWarnings("unchecked")
    public B putAll(@Nonnull Collection<Map.Entry<K, V>> entries) {
        if (entries.isEmpty()) return (B) this;
        for (Map.Entry<K, V> entry : entries) {
            checkNotNull(entry);
            put(requireNonNull(entry.getKey()), requireNonNull(entry.getValue()));
        }
        return (B) this;
    }
}
