package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.stream.Collector;

import static net.morimekta.util.collect.UnmodifiableCollection.checkNotNull;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableSet.makeHashTable;
import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static net.morimekta.util.collect.UnmodifiableSortedSet.sortedSetOf;

public class Unmodifiables {
    @Nonnull
    @SafeVarargs
    public static <E> UnmodifiableList<E> asList(E... elements) {
        if (elements.length == 0) return listOf();
        return new UnmodifiableList<>(Arrays.copyOf(elements, elements.length));
    }

    @Nonnull
    public static <E> Collector<E, ?, UnmodifiableList<E>> toList() {
        return new UnmodifiableListCollector<>();
    }

    @Nonnull
    public static <K, V> Collector<V, ?, UnmodifiableMap<K, V>>
    toMap(@Nonnull Function<V, K> toKey) {
        return new UnmodifiableMapCollector<>(toKey, i -> i);
    }

    @Nonnull
    public static <K, V, E> Collector<E, ?, UnmodifiableMap<K, V>>
    toMap(@Nonnull Function<E, K> toKey, @Nonnull Function<E, V> toValue) {
        return new UnmodifiableMapCollector<>(toKey, toValue);
    }

    @Nonnull
    public static <K, V> Collector<V, ?, UnmodifiableMap<K, List<V>>>
    groupingBy(@Nonnull Function<V, K> toKey) {
        return new UnmodifiableGroupingByCollector<>(toKey, v -> v);
    }

    @Nonnull
    public static <K, V> Collector<V, ?, UnmodifiableMap<K, List<V>>>
    groupingByAll(@Nonnull Function<V, Collection<K>> toKeys) {
        return new UnmodifiableGroupingByAllCollector<>(toKeys, v -> v);
    }

    @Nonnull
    @SafeVarargs
    public static <E> UnmodifiableSet<E> asSet(E... elements) {
        if (elements.length == 0) return setOf();
        checkNotNull(elements);
        Object[] copy = Arrays.copyOf(elements, elements.length);
        return new UnmodifiableSet<>(copy.length, copy, makeHashTable(copy, copy.length));
    }

    @Nonnull
    public static <E> Collector<E, ?, UnmodifiableSet<E>> toSet() {
        return new UnmodifiableSetCollector<>();
    }

    @Nonnull
    public static <K extends Comparable<K>, V>
    Collector<V, ?, UnmodifiableSortedMap<K, V>>
    toSortedMap(@Nonnull Function<V, K> toKey) {
        return new UnmodifiableSortedMapCollector<>(toKey, v -> v, null);
    }

    @Nonnull
    public static <K extends Comparable<K>, V, E>
    Collector<E, ?, UnmodifiableSortedMap<K, V>>
    toSortedMap(@Nonnull Function<E, K> toKey, @Nonnull Function<E, V> toValue) {
        return new UnmodifiableSortedMapCollector<>(toKey, toValue, null);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <K, V> Collector<V, ?, UnmodifiableSortedMap<K, V>>
    toSortedMap(@Nonnull Function<V, K> toKey, @Nonnull Comparator comparator) {
        return new UnmodifiableSortedMapCollector<>(toKey, v -> v, comparator);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <K, V, E> Collector<E, ?, UnmodifiableSortedMap<K, V>>
    toSortedMap(@Nonnull Function<E, K> toKey,
                @Nonnull Function<E, V> toValue,
                @Nonnull Comparator comparator) {
        return new UnmodifiableSortedMapCollector<>(toKey, toValue, comparator);
    }

    @SafeVarargs
    public static <E> UnmodifiableSortedSet<E> asSortedSet(E... values) {
        if (values.length == 0) return sortedSetOf();
        checkNotNull(values);
        Object[] copy = Arrays.copyOf(values, values.length);
        Arrays.sort(copy);
        return new UnmodifiableSortedSet<>(0, copy.length, copy, null);
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSortedSet<E> asSortedSet(@Nonnull Comparator<E> comparator, E... values) {
        if (values.length == 0) return sortedSetOf();
        checkNotNull(values);
        Object[] copy = Arrays.copyOf(values, values.length);
        Arrays.sort((E[]) copy, comparator);
        return new UnmodifiableSortedSet<>(0, copy.length, copy, comparator);
    }

    @Nonnull
    public static <T> Collector<T, ?, UnmodifiableSortedSet<T>> toSortedSet() {
        return new UnmodifiableSortedSetCollector<>(null);
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <T> Collector<T, ?, UnmodifiableSortedSet<T>> toSortedSet(Comparator comparator) {
        return new UnmodifiableSortedSetCollector<>(comparator);
    }

    @Nonnull
    public static <I, E> Set<E> mapToSet(@Nonnull Collection<I> from, @Nonnull Function<I, E> mapper) {
        if (from instanceof SortedSet) {
            return from.stream()
                       .map(mapper)
                       .collect(toSortedSet(((SortedSet) from).comparator()));
        } else {
            return from.stream()
                       .map(mapper)
                       .collect(toSet());
        }
    }

    @Nonnull
    public static <I, E> List<E> mapToList(@Nonnull Collection<I> from, @Nonnull Function<I, E> mapper) {
        return from.stream()
                   .map(mapper)
                   .collect(toList());
    }

    private Unmodifiables() {}
}
