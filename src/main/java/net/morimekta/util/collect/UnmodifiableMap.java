/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

import static java.lang.Integer.highestOneBit;
import static java.lang.Math.max;
import static java.util.Objects.requireNonNull;
import static net.morimekta.util.collect.UnmodifiableCollection.checkNotNull;

/**
 * Simple unmodifiable map that preserves entry order when iterating. This data
 * structure is essentially the same as an associative array, but not modifiable.
 * Backing lookup mechanism is a simple hash table.
 *
 * Note that the builder for this differs from the guava immutable map simply
 * by accepting values to be overwritten, and differs from the java.util
 * {@link java.util.HashMap} by being unmodifiable and order preserving, more
 * like an unmodifiable {@link java.util.LinkedHashMap}.
 *
 * Since the map is not modifiable it is possible to achieve full compatibility with
 * the {@link Map} interface with much simpler backing structures and logic. By
 * requiring all values to be non-null, some of the logic can be even simpler.
 *
 * @param <K> The entry key type.
 * @param <V> The entry value type.
 */
@Immutable
public final class UnmodifiableMap<K,V> extends UnmodifiableMapBase<K, V> implements Map<K, V> {
    // -------- Constructors --------

    @Nonnull
    public static <K, V> UnmodifiableMap<K, V> copyOf(Map<K, V> map) {
        if (map.isEmpty()) {
            return mapOf();
        }
        if (map instanceof UnmodifiableMap) {
            return (UnmodifiableMap<K, V>) map;
        }
        if (map instanceof UnmodifiableSortedMap) {
            UnmodifiableSortedMap<K, V> other = (UnmodifiableSortedMap<K, V>) map;
            return new UnmodifiableMap<>(other.size, other.entries, makeHashTable(other.entries, other.size));
        }

        UnmodifiableMap.Builder<K, V> builder = new Builder<>(map.size());
        builder.putAll(map);
        return builder.build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <K, V> UnmodifiableMap<K, V> mapOf() {
        return (UnmodifiableMap<K, V>) EMPTY;
    }

    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K key, @Nonnull V value) {
        return construct(entry(key, value));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2) {
        return construct(entry(k1, v1), entry(k2, v2));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3), entry(k4, v4));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3), entry(k4, v4), entry(k5, v5));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5,
                                                     @Nonnull K k6, @Nonnull V v6) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5,
                                                     @Nonnull K k6, @Nonnull V v6,
                                                     @Nonnull K k7, @Nonnull V v7) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5,
                                                     @Nonnull K k6, @Nonnull V v6,
                                                     @Nonnull K k7, @Nonnull V v7,
                                                     @Nonnull K k8, @Nonnull V v8) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5,
                                                     @Nonnull K k6, @Nonnull V v6,
                                                     @Nonnull K k7, @Nonnull V v7,
                                                     @Nonnull K k8, @Nonnull V v8,
                                                     @Nonnull K k9, @Nonnull V v9) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8),
                         entry(k9, v9));
    }
    public static <K, V> UnmodifiableMap<K, V> mapOf(@Nonnull K k1, @Nonnull V v1,
                                                     @Nonnull K k2, @Nonnull V v2,
                                                     @Nonnull K k3, @Nonnull V v3,
                                                     @Nonnull K k4, @Nonnull V v4,
                                                     @Nonnull K k5, @Nonnull V v5,
                                                     @Nonnull K k6, @Nonnull V v6,
                                                     @Nonnull K k7, @Nonnull V v7,
                                                     @Nonnull K k8, @Nonnull V v8,
                                                     @Nonnull K k9, @Nonnull V v9,
                                                     @Nonnull K k10, @Nonnull V v10) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8),
                         entry(k9, v9),
                         entry(k10, v10));
    }

    public static <K, V> UnmodifiableMap.Builder<K, V> builder() {
        return new Builder<>(4);
    }

    public static <K, V> UnmodifiableMap.Builder<K, V> builder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity));
    }

    // --- Map

    @Override
    public boolean containsKey(Object o) {
        if (o == null) return false;
        int hash = o.hashCode();
        for (int hp = hash & mask; ; hp = ++hp & mask) {
            Entry<K, V> p = hashTable[hp];
            if (p == null) {
                return false;
            }
            if (p.getKey().equals(o)) {
                return true;
            }
        }
    }

    @Override
    public V get(Object o) {
        if (o == null) return null;
        int hash = o.hashCode();
        for (int hp = hash & mask; ; hp = ++hp & mask) {
            Entry<K, V> p = hashTable[hp];
            if (p == null) {
                return null;
            }
            if (p.getKey().equals(o)) {
                return p.getValue();
            }
        }
    }

    // -------- UnmodifiableMapBase --------

    public final static class Builder<K, V> extends UnmodifiableMapBuilder<K, V, UnmodifiableMap<K, V>, Builder<K, V>> {
        private Entry<K, V>[] entries;
        private int           size;
        private Entry<K, V>[] hashTable;
        private int           mask;

        @SuppressWarnings("unchecked")
        Builder(int initialSize) {
            entries = new Entry[initialSize];
            size = 0;
            hashTable = new Entry[hashTableSizeFor(initialSize, MIN_HASH_TABLE_SIZE)];
            mask = hashTable.length - 1;
        }

        @Nonnull
        @Override
        public Builder<K, V> put(@Nonnull K key, @Nonnull V value) {
            checkNotNull(key);
            checkNotNull(value);
            ensureCapacity(size + 1);
            findAndInsert(key, value);
            return this;
        }

        @Nonnull
        @Override
        public Builder<K, V> putAll(@Nonnull Map<? extends K, ? extends V> map) {
            checkNotNull(map);
            ensureCapacity(size + map.size());
            for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
                findAndInsert(requireNonNull(entry.getKey()),
                              requireNonNull(entry.getValue()));
            }
            return this;
        }

        @Override
        public UnmodifiableMap<K, V> build() {
            if (size == 0) {
                return mapOf();
            }
            @SuppressWarnings("unchecked")
            Entry<K, V>[] array = new Entry[size];
            for (int i = 0; i < size; ++i) {
                array[i] = new AbstractMap.SimpleImmutableEntry<>(entries[i].getKey(),
                                                                  entries[i].getValue());
            }
            Entry<K, V>[] table = makeHashTable(array, size);
            return new UnmodifiableMap<>(size, array, table);
        }

        private void ensureCapacity(int minSize) {
            if (minSize > entries.length) {
                @SuppressWarnings("unchecked")
                Entry<K, V>[] array = new Entry[max(minSize, entries.length * 2)];
                System.arraycopy(entries, 0, array, 0, size);
                entries = array;
                hashTable = makeHashTable(entries, size,
                                          hashTableSizeFor(entries.length, hashTable.length));
                mask = hashTable.length - 1;
            }
        }

        private void findAndInsert(K key, V value) {
            int hash = key.hashCode();
            for (int hp = hash & mask; ; hp = ++hp & mask) {
                Entry<K, V> p = hashTable[hp];
                if (p == null) {
                    p = new AbstractMap.SimpleEntry<>(key, value);
                    hashTable[hp] = p;
                    entries[size++] = p;
                    break;
                } else if (p.getKey().equals(key)) {
                    p.setValue(value);
                    break;
                }
            }
        }
    }

    @Nonnull
    @Override
    protected Set<Entry<K, V>> makeEntrySet() {
        if (size == 0) return UnmodifiableSet.setOf();
        return new UnmodifiableSet<>(size, entries, UnmodifiableSet.makeHashTable(entries, size));
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    protected Set<K> makeKeySet() {
        if (size == 0) return UnmodifiableSet.setOf();
        Object[] keys = new Object[size];
        for (int i = 0; i < size; ++i) {
            keys[i] = entries[i].getKey();
        }
        return (Set<K>) UnmodifiableSet.construct(keys);
    }

    // -------- Private --------

    UnmodifiableMap(int size, Entry<K, V>[] entries, Entry<K, V>[] hashTable) {
        super(size, entries);
        this.hashTable = hashTable;
        this.mask = hashTable.length - 1;
    }

    private static int hashTableSizeFor(final int arraySize, int currentHashTableSize) {
        int tableSize = max(highestOneBit(arraySize) << 1, max(MIN_HASH_TABLE_SIZE, currentHashTableSize));
        if (tableSize < (int) (MIN_OVER_CAPACITY * (double) arraySize)) {
            tableSize <<= 1;
        }
        return tableSize;
    }

    @SafeVarargs
    private static <K, V> UnmodifiableMap<K, V> construct(Entry<K, V>... entries) {
        return new UnmodifiableMap<>(entries.length, entries, makeHashTable(entries, entries.length));
    }

    private static <K, V> Entry<K, V>[] makeHashTable(Entry<K, V>[] array, int size) {
        return makeHashTable(array, size, hashTableSizeFor(size, MIN_HASH_TABLE_SIZE));
    }

    private static <K, V> Entry<K, V>[] makeHashTable(Entry<K, V>[] array, int size, int hashTableSize) {
        int mask = hashTableSize - 1;
        @SuppressWarnings("unchecked")
        Entry<K, V>[] hashTable = new Entry[hashTableSize];
        for (int i = 0; i < size; ++i) {
            Entry<K, V> o = array[i];
            int hash = o.getKey().hashCode();
            int hp   = hash & mask;
            for (; ; ) {
                Entry<K, V> p = hashTable[hp];
                if (p == null) {
                    hashTable[hp] = o;
                    break;
                }
                hp = ++hp & mask;
            }
        }

        return hashTable;
    }

    private static final double MIN_OVER_CAPACITY = 1.38;
    private static final int MIN_HASH_TABLE_SIZE = 2;

    @SuppressWarnings("unchecked")
    private static final UnmodifiableMap<Object,Object> EMPTY = new UnmodifiableMap<>(0, NO_ENTRIES, new Entry[MIN_HASH_TABLE_SIZE]);

    private final transient Entry<K, V>[] hashTable;
    private final transient int mask;

}
