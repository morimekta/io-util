/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import static java.lang.Integer.highestOneBit;
import static java.lang.Math.max;

/**
 * Simple immutable set. This set is order-preserving, so when iterating
 * the elements, they will come in the same order as added. If same value
 * was added multiple times, the first entry counts.
 *
 * @param <E> The set element type.
 */
@Immutable
public final class UnmodifiableSet<E> extends UnmodifiableCollection<E> implements Set<E> {
    // -------- Constructors --------

    @Nonnull
    public static <E> UnmodifiableSet<E> copyOf(E[] array) {
        if (array.length == 0) return setOf();
        Object[] copy = Arrays.copyOf(array, array.length);
        return new UnmodifiableSet<>(copy.length, copy, makeHashTable(copy, copy.length));
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSet<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection<E>) iterable);
        }
        return copyOf(iterable.iterator());
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> copyOf(Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) return setOf();
        return UnmodifiableSet.<E>builder().addAll(iterator).build();
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> copyOf(Enumeration<? extends E> enumeration) {
        if (!enumeration.hasMoreElements()) return setOf();
        return UnmodifiableSet.<E>builder().addAll(enumeration).build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSet<E> copyOf(Collection<? extends E> collection) {
        if (collection.size() == 0) return setOf();
        if (collection instanceof UnmodifiableSet) {
            return (UnmodifiableSet) collection;
        }
        if (collection instanceof UnmodifiableSortedSet) {
            UnmodifiableSortedSet<E> set = (UnmodifiableSortedSet) collection;
            return new UnmodifiableSet<>(set.length, set.array, makeHashTable(set.array, set.length));
        }
        return new UnmodifiableSet.Builder<E>(collection.size()).addAll(collection).build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableSet<E> setOf() {
        return (UnmodifiableSet<E>) EMPTY;
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e) {
        return construct(e);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2) {
        return construct(e1, e2);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3) {
        return construct(e1, e2, e3);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4) {
        return construct(e1, e2, e3, e4);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5) {
        return construct(e1, e2, e3, e4, e5);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5, E e6) {
        return construct(e1, e2, e3, e4, e5, e6);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7) {
        return construct(e1, e2, e3, e4, e5, e6, e7);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8);
    }

    @Nonnull
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9);
    }

    @Nonnull
    @SafeVarargs
    public static <E> UnmodifiableSet<E> setOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E... more) {
        return UnmodifiableSet.<E>builder(10 + more.length)
                .add(e1)
                .add(e2)
                .add(e3)
                .add(e4)
                .add(e5)
                .add(e6)
                .add(e7)
                .add(e8)
                .add(e9)
                .add(e10)
                .addAll(more)
                .build();
    }

    @Nonnull
    public static <E> UnmodifiableSet.Builder<E> builder() {
        return new UnmodifiableSet.Builder<>(4);
    }

    @Nonnull
    public static <E> Builder<E> builder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity));
    }

    // -------- Object --------

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Set)) return false;
        Set other = (Set) obj;
        if (other.size() != length) return false;
        for (Object o : (Set) obj) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        for (int i = offset; i < (offset + length); ++i) {
            if (i > 0) builder.append(", ");
            builder.append(array[i].toString());
        }
        return builder.append("}").toString();
    }

    @Override
    public int hashCode() {
        if (hashCode == null) {
            int hash = Objects.hash(getClass(), length);
            for (int i = offset; i < (offset + length); ++i) {
                hash ^= Objects.hash(array[i]);
            }
            hashCode = hash;
        }
        return hashCode;
    }

    // -------- Collection --------

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        int hash = o.hashCode();
        for (int hp = hash & mask; ; hp = ++hp & mask) {
            Object p = hashTable[hp];
            if (p == null) return false;
            if (p.equals(o)) return true;
        }
    }

    // -------- UnmodifiableSet --------

    public final static class Builder<E> implements UnmodifiableCollectionBuilder<E, UnmodifiableSet<E>, Builder<E>> {
        private Object[] array;
        private int      size;
        private Object[] hashTable;
        private int      mask;
        private boolean  forceRehash;

        Builder(int initialSize) {
            array = new Object[initialSize];
            size = 0;
            hashTable = new Object[hashTableSizeFor(array.length, MIN_HASH_TABLE_SIZE)];
            mask = hashTable.length - 1;
            forceRehash = false;
        }

        @Nonnull
        @Override
        public Builder<E> add(@Nonnull E e) {
            checkNotNull(e);
            ensureCapacity(size + 1);
            hashTableAdd(e);
            return this;
        }

        @Nonnull
        @Override
        @SafeVarargs
        public final Builder<E> addAll(E... items) {
            if (items.length == 0) {
                return this;
            }
            checkNotNull(items);
            ensureCapacity(size + items.length);
            for (E e : items) {
                hashTableAdd(e);
            }
            return this;
        }

        @Nonnull
        @Override
        public Builder<E> addAll(@Nonnull Collection<? extends E> collection) {
            if (collection.isEmpty()) {
                return this;
            }
            ensureCapacity(size + collection.size());
            for (E e : collection) {
                hashTableAdd(e);
            }
            return this;
        }

        @Override
        public UnmodifiableSet<E> build() {
            forceRehash = true;  // In case the builder is continuously used.
            return new UnmodifiableSet<>(size, array, hashTable);
        }

        private void ensureCapacity(int capacity) {
            if (array.length < capacity) {
                Object[] old = array;
                array = new Object[max(capacity, array.length * 2)];
                System.arraycopy(old, 0, array, 0, size);
                forceRehash = true;
            }
            if (forceRehash) {
                hashTable = makeHashTable(array, size, hashTableSizeFor(array.length, hashTable.length));
                mask = hashTable.length - 1;
                forceRehash = false;
            }
        }

        private void hashTableAdd(Object e) {
            int hash = e.hashCode();
            for (int hp = hash & mask;; hp = ++hp & mask) {
                Object p = hashTable[hp];
                if (p == null) {
                    hashTable[hp] = e;
                    array[size++] = e;
                    break;
                } else if (p.equals(e)) {
                    break;
                }
            }
        }
    }

    // -------- Private --------

    UnmodifiableSet(int size, Object[] array, Object[] hashTable) {
        super(0, size, array);
        this.hashTable = hashTable;
        this.mask = hashTable.length - 1;
    }

    @Nonnull
    @SafeVarargs
    static <E> UnmodifiableSet<E> construct(E... array) {
        return UnmodifiableSet.<E>builder(array.length).addAll(array).build();
    }

    private static int hashTableSizeFor(final int arraySize, int currentHashTableSize) {
        int tableSize = max(highestOneBit(arraySize) << 1, max(MIN_HASH_TABLE_SIZE, currentHashTableSize));
        if (tableSize < (int) (MIN_OVER_CAPACITY * (double) arraySize)) {
            tableSize <<= 1;
        }
        return tableSize;
    }

    static Object[] makeHashTable(Object[] array, int size) {
        return makeHashTable(array, size, hashTableSizeFor(size, MIN_HASH_TABLE_SIZE));
    }

    private static Object[] makeHashTable(Object[] array, int size, int hashTableSize) {
        int mask = hashTableSize - 1;
        Object[] hashTable = new Object[hashTableSize];
        for (int i = 0; i < size; ++i) {
            Object o = array[i];
            // if (o == null) throw new NullPointerException("Null item at index " + i + " of " + size);
            int hash = o.hashCode();
            int hp   = hash & mask;
            for (; ; ) {
                Object p = hashTable[hp];
                if (p == null) {
                    hashTable[hp] = o;
                    break;
                }
                hp = ++hp & mask;
            }
        }

        return hashTable;
    }

    private static final double MIN_OVER_CAPACITY = 1.38;
    private static final int MIN_HASH_TABLE_SIZE = 2;

    private static final UnmodifiableSet<Object> EMPTY = new UnmodifiableSet<>(
            0, EMPTY_ARRAY, new Object[MIN_HASH_TABLE_SIZE]);

    private final transient Object[] hashTable;
    private final transient int      mask;

}
