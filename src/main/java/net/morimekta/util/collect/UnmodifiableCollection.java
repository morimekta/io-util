/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.function.Predicate;

import static net.morimekta.util.collect.UnmodifiableList.listOf;

/**
 * Simple unmodifiable collection base class. Note that these are called
 * 'unmodifiable', not 'immutable', as whether they are truly immutable
 * relies on the containing items being immutable classes too. But in
 * that case it <b>is</b> immutable, therefore the {@link javax.annotation.concurrent.Immutable}
 * annotations on all it's implementations.
 *
 * @param <E> The element type of the collection.
 */
public abstract class UnmodifiableCollection<E> implements Collection<E> {
    /**
     * Get the collection as an unmodifiable random access list.
     *
     * @return The unmodifiable list.
     */
    @Nonnull
    public UnmodifiableList<E> asList() {
        if (asList == null) {
            if (length == 0) asList = listOf();
            else asList = new UnmodifiableList<>(offset, length, array);
        }
        return asList;
    }

    // -------- Collection --------

    @Nonnull
    @Override
    public Object[] toArray() {
        return toArray(EMPTY_ARRAY);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(@Nonnull T[] ts) {
        T[] out = (T[]) Array.newInstance(ts.getClass().getComponentType(), length);
        if (length > 0) {
            System.arraycopy(array, offset, out, 0, length);
        }
        return out;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return length == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        for (int i = offset; i < (offset + length); ++i) {
            if (array[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl(offset);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> collection) {
        if (isEmpty() && !collection.isEmpty()) return false;
        for (Object o : collection) {
            if (!contains(o)) return false;
        }
        return true;
    }

    // -------- Private --------

    static final Object[] EMPTY_ARRAY = new Object[0];

    protected transient final Object[] array;
    protected transient final int length;
    protected transient final int offset;

    @Nullable
    private transient UnmodifiableList<E> asList;
    @Nullable
    transient Integer             hashCode;

    UnmodifiableCollection(int offset, int length, Object[] array) {
        if (offset < 0 || length < 0) throw new IllegalArgumentException();
        if (offset + length > array.length) throw new IllegalArgumentException();
        this.offset = offset;
        this.length = length;
        this.array = array;
    }

    // -------- ITERATOR --------

    class IteratorImpl implements ListIterator<E> {
        private int next;

        IteratorImpl(int index) {
            this.next = index;
        }

        @Override
        public boolean hasNext() {
            return next < (offset + length);
        }

        @Override
        @SuppressWarnings("unchecked")
        public E next() {
            if (next >= (offset + length)) throw new IndexOutOfBoundsException("Index " + (next - offset) + " out of bounds for length " + length);
            ++next;
            return (E) array[next - 1];
        }

        @Override
        public boolean hasPrevious() {
            return (next - offset) > 0;
        }

        @Override
        @SuppressWarnings("unchecked")
        public E previous() {
            if ((next - offset - 1) < 0) throw new IndexOutOfBoundsException("Index -1 out of bounds for length " + length);
            --next;
            return (E) array[next];
        }

        @Override
        public int nextIndex() {
            return next - offset;
        }

        @Override
        public int previousIndex() {
            return next - offset - 1;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Operation not allowed");
        }

        @Override
        public void set(E e) {
            throw new UnsupportedOperationException("Operation not allowed");
        }

        @Override
        public void add(E e) {
            throw new UnsupportedOperationException("Operation not allowed");
        }
    }

    // -------- UTILITIES --------

    static void checkNotNull(Object o) {
        if (o == null) throw new NullPointerException();
    }

    static void checkNotNull(Object[] array) {
        if (array == null) throw new NullPointerException();
        for (Object o : array) {
            if (o == null) throw new NullPointerException();
        }
    }

    static void checkNotNull(Collection collection) {
        if (collection == null) throw new NullPointerException();
        for (Object o : collection) {
            if (o == null) throw new NullPointerException();
        }
    }

    // -------- UNSUPPORTED --------

    @Override
    public boolean add(E e) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public boolean addAll(@Nonnull Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public boolean removeAll(@Nonnull Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public boolean removeIf(@Nonnull Predicate<? super E> filter) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> collection) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Operation not allowed");
    }
}
