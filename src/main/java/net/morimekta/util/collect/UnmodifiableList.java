/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.RandomAccess;

import static java.lang.Math.max;

/**
 * Simple unmodifiable list implementation backed by an object array.
 *
 * @param <E> The list item type.
 */
@Immutable
public final class UnmodifiableList<E> extends UnmodifiableCollection<E> implements List<E>, RandomAccess {
    // -------- Constructors --------

    @Nonnull
    public static <E> UnmodifiableList<E> copyOf(E[] array) {
        if (array.length == 0) {
            return listOf();
        }
        return new UnmodifiableList<>(Arrays.copyOf(array, array.length));
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableList<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof UnmodifiableCollection) {
            return ((UnmodifiableCollection<E>) iterable).asList();
        } else if (iterable instanceof Collection) {
            return copyOf((Collection<E>) iterable);
        }
        return copyOf(iterable.iterator());
    }

    @Nonnull
    public static <E> UnmodifiableList<E> copyOf(Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) {
            return listOf();
        }
        return UnmodifiableList.<E>builder().addAll(iterator).build();
    }

    @Nonnull
    public static <E> UnmodifiableList<E> copyOf(Enumeration<? extends E> enumeration) {
        if (!enumeration.hasMoreElements()) {
            return listOf();
        }
        return UnmodifiableList.<E>builder().addAll(enumeration).build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableList<E> copyOf(Collection<? extends E> collection) {
        if (collection instanceof UnmodifiableCollection) {
            return ((UnmodifiableCollection<E>) collection).asList();
        }
        if (collection.size() == 0) {
            return listOf();
        }
        return new Builder<E>(collection.size()).addAll(collection).build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <E> UnmodifiableList<E> listOf() {
        return (UnmodifiableList<E>) EMPTY;
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e) {
        return construct(e);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2) {
        return construct(e1, e2);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3) {
        return construct(e1, e2, e3);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4) {
        return construct(e1, e2, e3, e4);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5) {
        return construct(e1, e2, e3, e4, e5);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5, E e6) {
        return construct(e1, e2, e3, e4, e5, e6);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7) {
        return construct(e1, e2, e3, e4, e5, e6, e7);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8);
    }

    @Nonnull
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9) {
        return construct(e1, e2, e3, e4, e5, e6, e7, e8, e9);
    }

    @Nonnull
    @SafeVarargs
    public static <E> UnmodifiableList<E> listOf(E e1, E e2, E e3, E e4, E e5, E e6, E e7, E e8, E e9, E e10, E... more) {
        Object[] array = new Object[10 + more.length];
        array[0] = e1;
        array[1] = e2;
        array[2] = e3;
        array[3] = e4;
        array[4] = e5;
        array[5] = e6;
        array[6] = e7;
        array[7] = e8;
        array[8] = e9;
        array[9] = e10;
        System.arraycopy(more, 0, array, 10, more.length);
        return new UnmodifiableList<>(array);
    }

    @Nonnull
    public static <E> Builder<E> builder() {
        return new Builder<>(4);
    }

    @Nonnull
    public static <E> Builder<E> builder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity));
    }

    // -------- Object --------

    @Override
    public int hashCode() {
        if (hashCode == null) {
            int hash = Objects.hash(getClass(), length);
            for (int i = 0; i < length; ++i) {
                hash ^= Objects.hash(i, array[offset + i]);
            }
            hashCode = hash;
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (length != ((List) obj).size()) {
            return false;
        }
        if (obj instanceof UnmodifiableList) {
            UnmodifiableList other = (UnmodifiableList) obj;
            if (offset == other.offset && array == other.array) {
                return true;
            }
            for (int i = 0; i < length; ++i) {
                if (!array[offset + i].equals(other.array[other.offset + i])) {
                    return false;
                }
            }
            return true;
        } else if (obj instanceof RandomAccess) {
            List other = (List) obj;
            for (int i = 0; i < length; ++i) {
                if (!array[offset + i].equals(other.get(i))) {
                    return false;
                }
            }
            return true;
        } else {
            List other = (List) obj;
            int i = offset;
            for (Object o : other) {
                if (!array[i++].equals(o)) {
                    return false;
                }
            }
            return true;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("[");
        for (int i = offset; i < (offset + length); ++i) {
            if (i > 0) builder.append(", ");
            builder.append(array[i].toString());
        }
        return builder.append("]").toString();
    }

    // -------- List --------

    @Override
    public boolean addAll(int i, @Nonnull Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int i) {
        return (E) array[offset + i];
    }

    @Override
    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation not allowed");

    }

    @Override
    public E remove(int i) {
        throw new UnsupportedOperationException("Operation not allowed");
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) return -1;
        for (int i = offset; i < (offset + length); ++i) {
            if (o.equals(array[i])) return i - offset;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o == null) return -1;
        for (int i = (offset + length) - 1; i >= offset; --i) {
            if (o.equals(array[i])) return i - offset;
        }
        return -1;
    }

    @Nonnull
    @Override
    public ListIterator<E> listIterator() {
        return new IteratorImpl(0);
    }

    @Nonnull
    @Override
    public ListIterator<E> listIterator(int i) {
        if (i < 0 || i > length) {
            throw new IndexOutOfBoundsException("ListIterator index " + i + " out of bounds for length " + array.length);
        }
        return new IteratorImpl(i);
    }

    @Nonnull
    @Override
    public List<E> subList(int start, int end) {
        if (start == 0 && end == length) {
            return this;
        }
        if (start < 0 || start > length) {
            throw new IndexOutOfBoundsException("Start index " + start + " out of bounds for length " + length);
        }
        if (end < start || end > length) {
            throw new IndexOutOfBoundsException("End index " + end + " out of bounds for length " + length + ", start " + start);
        }
        int len = end - start;
        if (len == 0) {
            return listOf();
        }
        return new UnmodifiableList<>(offset + start, len, array);
    }

    // -------- UnmodifiableCollection --------

    @Nonnull
    @Override
    public UnmodifiableList<E> asList() {
        return this;
    }

    // -------- Builder --------

    public final static class Builder<E> implements UnmodifiableCollectionBuilder<E, UnmodifiableList<E>, Builder<E>> {
        private Object[] array;
        private int size;

        Builder(int initialCapacity) {
            this.array = new Object[initialCapacity];
            this.size = 0;
        }

        @Nonnull
        public Builder<E> add(@Nonnull E e) {
            checkNotNull(e);
            ensureCapacity(size + 1);
            array[size++] = e;
            return this;
        }

        @Nonnull
        public Builder<E> addAll(@Nonnull Collection<? extends E> collection) {
            if (collection.isEmpty()) {
                return this;
            }
            checkNotNull(collection);
            ensureCapacity(size + collection.size());
            for (Object o : collection) {
                array[size++] = o;
            }
            return this;
        }

        @Nonnull
        @SafeVarargs
        public final Builder<E> addAll(E... items) {
            if (items.length == 0) {
                return this;
            }
            checkNotNull(items);
            ensureCapacity(size + items.length);
            System.arraycopy(items, 0, array, size, items.length);
            size += items.length;
            return this;
        }

        @Nonnull
        public UnmodifiableList<E> build() {
            if (size == 0) return listOf();
            return new UnmodifiableList<>(0, size, array);
        }

        private void ensureCapacity(int capacity) {
            if (array.length < capacity) {
                Object[] old = array;
                array = new Object[array.length * 2];
                System.arraycopy(old, 0, array, 0, size);
            }
        }
    }

    // -------- Private --------

    UnmodifiableList(Object[] array) {
        super(0, array.length, array);
    }
    UnmodifiableList(int offset, int length, Object[] array) {
        super(offset, length, array);
    }

    @Nonnull
    @SafeVarargs
    private static <E> UnmodifiableList<E> construct(E... array) {
        return new UnmodifiableList<>(array);
    }

    private static final UnmodifiableList<Object> EMPTY = new UnmodifiableList<>(new Object[0]);

}
