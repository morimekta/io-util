/*
 * Copyright (C) 2018 Stein Eldar Johnsen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.morimekta.util.collect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import static java.lang.Math.max;
import static java.util.Arrays.copyOfRange;

@Immutable
public final class UnmodifiableSortedMap<K, V> extends UnmodifiableMapBase<K, V> implements SortedMap<K, V> {
    // -------- Constructors --------

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> copyOf(Map<K, V> map) {
        if (map.isEmpty()) {
            return sortedMapOf();
        }
        if (map instanceof UnmodifiableSortedMap) {
            return (UnmodifiableSortedMap<K, V>) map;
        }
        Comparator comparator = null;
        if (map instanceof SortedMap) {
            comparator = ((SortedMap<K, V>) map).comparator();
        }
        UnmodifiableSortedMap.Builder<K, V> builder = new Builder<>(map.size(), comparator);
        builder.putAll(map);
        return builder.build();
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf() {
        return (UnmodifiableSortedMap<K, V>) EMPTY;
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K key, @Nonnull V value) {
        return construct(entry(key, value));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1, @Nonnull K k2, @Nonnull V v2) {
        return construct(entry(k1, v1), entry(k2, v2));
    }


    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3), entry(k4, v4));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5) {
        return construct(entry(k1, v1), entry(k2, v2), entry(k3, v3), entry(k4, v4), entry(k5, v5));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5,
                                                                 @Nonnull K k6, @Nonnull V v6) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5,
                                                                 @Nonnull K k6, @Nonnull V v6,
                                                                 @Nonnull K k7, @Nonnull V v7) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5,
                                                                 @Nonnull K k6, @Nonnull V v6,
                                                                 @Nonnull K k7, @Nonnull V v7,
                                                                 @Nonnull K k8, @Nonnull V v8) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5,
                                                                 @Nonnull K k6, @Nonnull V v6,
                                                                 @Nonnull K k7, @Nonnull V v7,
                                                                 @Nonnull K k8, @Nonnull V v8,
                                                                 @Nonnull K k9, @Nonnull V v9) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8),
                         entry(k9, v9));
    }

    @Nonnull
    public static <K, V> UnmodifiableSortedMap<K, V> sortedMapOf(@Nonnull K k1, @Nonnull V v1,
                                                                 @Nonnull K k2, @Nonnull V v2,
                                                                 @Nonnull K k3, @Nonnull V v3,
                                                                 @Nonnull K k4, @Nonnull V v4,
                                                                 @Nonnull K k5, @Nonnull V v5,
                                                                 @Nonnull K k6, @Nonnull V v6,
                                                                 @Nonnull K k7, @Nonnull V v7,
                                                                 @Nonnull K k8, @Nonnull V v8,
                                                                 @Nonnull K k9, @Nonnull V v9,
                                                                 @Nonnull K k10, @Nonnull V v10) {
        return construct(entry(k1, v1),
                         entry(k2, v2),
                         entry(k3, v3),
                         entry(k4, v4),
                         entry(k5, v5),
                         entry(k6, v6),
                         entry(k7, v7),
                         entry(k8, v8),
                         entry(k9, v9),
                         entry(k10, v10));
    }

    @Nonnull
    public static <K, V> Builder<K, V> builderOrderedBy(Comparator<K> comparator) {
        return new Builder<>(4, comparator);
    }

    @Nonnull
    public static <K, V> Builder<K, V> builderOrderedBy(int initialCapacity, Comparator<K> comparator) {
        return new Builder<>(max(1, initialCapacity), comparator);
    }

    @Nonnull
    public static <K extends Comparable<K>, V> Builder<K, V> builderNaturalOrder() {
        return new Builder<>(4, null);
    }

    @Nonnull
    public static <K extends Comparable<K>, V> Builder<K, V> builderNaturalOrder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity), null);
    }
    @Nonnull
    public static <K extends Comparable<K>, V> Builder<K, V> builderReverseOrder() {
        return new Builder<>(4, Comparator.reverseOrder());
    }

    @Nonnull
    public static <K extends Comparable<K>, V> Builder<K, V> builderReverseOrder(int initialCapacity) {
        return new Builder<>(max(1, initialCapacity), Comparator.reverseOrder());
    }

    // -------- Map --------

    @Override
    @SuppressWarnings("unchecked")
    public boolean containsKey(Object o) {
        return findIndex((K) o) >= 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public V get(Object o) {
        Entry<K, V> entry = find((K) o);
        if (entry != null) return entry.getValue();
        return null;
    }

    // -------- SortedMap --------

    @Override
    public Comparator<K> comparator() {
        return comparator;
    }

    @Nonnull
    @Override
    public UnmodifiableSortedMap<K, V> subMap(K start, K end) {
        int si = findIndex(start);
        if (si < 0) {
            si = -1 - si;
        }
        int ei = findIndex(end);
        if (ei < 0) {
            ei = -1 - ei;
        }
        int len = ei - si;
        if (len <= 0) return sortedMapOf();
        if (si == 0 && ei == size) return this;
        if (si == 0) {
            return new UnmodifiableSortedMap<>(len, entries, comparator, entryComparator);
        }
        return new UnmodifiableSortedMap<>(len, copyOfRange(entries, si, ei), comparator, entryComparator);
    }

    @Nonnull
    @Override
    public UnmodifiableSortedMap<K, V> headMap(K end) {
        int ei = findIndex(end);
        if (ei < 0) {
            ei = -1 - ei;
        }
        if (ei == 0) return sortedMapOf();
        if (ei == size) return this;
        return new UnmodifiableSortedMap<>(ei, entries, comparator, entryComparator);
    }

    @Nonnull
    @Override
    public UnmodifiableSortedMap<K, V> tailMap(K start) {
        int si = findIndex(start);
        if (si < 0) {
            si = -1 - si;
        }
        if (si == 0) {
            return this;
        }
        int len = size - si;
        if (len <= 0) {
            return sortedMapOf();
        }
        return new UnmodifiableSortedMap<>(len, copyOfRange(entries, si, size), comparator, entryComparator);
    }

    @Override
    public K firstKey() {
        return size > 0 ? entries[0].getKey() : null;
    }

    @Override
    public K lastKey() {
        return size > 0 ? entries[size - 1].getKey() : null;
    }

    // -------- UnmodifiableMapBase --------

    public final static class Builder<K, V> extends UnmodifiableMapBuilder<K, V, UnmodifiableSortedMap<K, V>, Builder<K, V>> {
        private Entry<K, V>[]               array;
        private int                         size;
        private UnmodifiableSortedMap<K, V> justBuilt;

        private final Comparator<K> comparator;
        private final Comparator<Entry<K, V>> entryComparator;

        @SuppressWarnings("unchecked")
        Builder(int initialCapacity, Comparator comparator) {
            this.array = new Entry[initialCapacity];
            this.size = 0;
            this.comparator = comparator;
            this.entryComparator = makeEntryComparator(comparator);
            this.justBuilt = null;
        }

        @Nonnull
        @Override
        public Builder<K, V> put(@Nonnull K key, @Nonnull V value) {
            ensureCapacity(size + 1);
            array[size++] = new AbstractMap.SimpleImmutableEntry<>(key, value);
            return this;
        }

        @Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public Builder<K, V> putAll(@Nonnull Map<? extends K, ? extends V> map) {
            if (map.isEmpty()) return this;
            ensureCapacity(size + map.size());
            if (map instanceof UnmodifiableMapBase) {
                UnmodifiableMapBase<? extends K, ? extends V> base = (UnmodifiableMapBase<? extends K, ? extends V>) map;
                for (int i = 0; i < base.size; ++i) {
                    array[size++] = (Entry) base.entries[i];
                }
            } else {
                for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
                    array[size++] = new AbstractMap.SimpleImmutableEntry<>(entry.getKey(), entry.getValue());
                }
            }
            return this;
        }

        @Override
        public UnmodifiableSortedMap<K, V> build() {
            if (size == 0) return sortedMapOf();
            if (justBuilt != null) return justBuilt;
            makeSorted(1);
            justBuilt = new UnmodifiableSortedMap<>(size, array, comparator, entryComparator);
            return justBuilt;
        }

        @SuppressWarnings("unchecked")
        private void makeSorted(int minSize) {
            if (size == 0) return;

            // A: Sort
            Arrays.sort(array, 0, size, entryComparator);

            // B: Deduplicate
            final int originalSize = size;
            for (int i = 1; i < originalSize; ++i) {
                if (array[i].getKey().equals(array[i - 1].getKey())) {
                    // The last entry is kept.
                    array[i - 1] = null;
                    --size;
                }
            }

            // C: compact away null values.
            if (size < originalSize) {
                if (array.length - max(size, minSize) > 1024) {
                    // In order to save memory, minimize larger arrays.
                    Entry[] compact = new Entry[max(minSize, size)];
                    int pos = 0;
                    for (int i = 0; i < originalSize; ++i) {
                        Entry o = array[i];
                        if (o != null) {
                            compact[pos++] = o;
                        }
                    }
                    array = compact;
                } else {
                    for (int moveTo = 0, moveFrom = 1;
                         moveTo < size && moveFrom < originalSize;
                         ++moveTo, ++moveFrom) {
                        if (array[moveTo] == null) {
                            while (array[moveFrom] == null) ++moveFrom;
                            array[moveTo] = array[moveFrom];
                            array[moveFrom] = null;
                        }
                    }
                }
            }
        }

        @SuppressWarnings("unchecked")
        private void ensureCapacity(int newSize) {
            if (array.length < newSize) {
                Entry[] old = array;
                array = new Entry[old.length * 2];
                System.arraycopy(old, 0, array, 0, size);
                makeSorted(newSize);
            } else if (justBuilt != null) {
                Entry[] old = array;
                array = new Entry[old.length];
                System.arraycopy(old, 0, array, 0, size);
                // Already sorted.
            }
            justBuilt = null;
        }
    }

    @Nonnull
    @Override
    protected Set<Entry<K, V>> makeEntrySet() {
        if (size == 0) return UnmodifiableSortedSet.sortedSetOf();
        return new UnmodifiableSortedSet<>(0, size, entries, entryComparator);
    }

    @Nonnull
    @Override
    protected Set<K> makeKeySet() {
        if (size == 0) return UnmodifiableSortedSet.sortedSetOf();
        Object[] keys = new Object[size];
        for (int i = 0; i < size; ++i) {
            keys[i] = entries[i].getKey();
        }
        return new UnmodifiableSortedSet<>(0, size, keys, comparator);
    }

    // -------- Private --------

    private UnmodifiableSortedMap(int size, @Nonnull Entry<K, V>[] entries, Comparator<K> comparator) {
        this(size, entries, comparator, makeEntryComparator(comparator));
    }

    UnmodifiableSortedMap(int size,
                          @Nonnull Entry<K, V>[] entries,
                          @Nullable Comparator<K> comparator,
                          @Nonnull Comparator<Entry<K, V>> entryComparator) {
        super(size, entries);
        this.comparator = comparator;
        this.entryComparator = entryComparator;
    }

    private Entry<K, V> find(K key) {
        int index = findIndex(key);
        if (index >= 0) return entries[index];
        return null;
    }

    private int findIndex(K key) {
        int insertPos = Arrays.binarySearch(entries, 0, size, new AbstractMap.SimpleImmutableEntry<>(key, null), entryComparator);
        int candiate = -1 - insertPos;
        if (candiate == size) return insertPos;
        if (entries[candiate].getKey().equals(key)) return candiate;
        return insertPos;
    }

    @SafeVarargs
    private static <K, V> UnmodifiableSortedMap<K, V> construct(Entry<K, V>... entries) {
        Comparator<Entry<K, V>> entryComparator = makeEntryComparator(null);
        Arrays.sort(entries, entryComparator);
        return new UnmodifiableSortedMap<>(entries.length, entries, null, entryComparator);
    }

    static <K, V> Comparator<Entry<K, V>> makeEntryComparator(Comparator<K> comparator) {
        @SuppressWarnings("unchecked")
        Comparator<Entry<K, V>> base = comparator == null ?
                                       Comparator.comparing(Entry::getKey, (Comparator<K>) Comparator.naturalOrder()) :
                                       Comparator.comparing(Entry::getKey, comparator);
        // This should make it sort null values before non-null.
        // This is used to get the correct binary search behavior based on null values in search key.
        return base.thenComparing(e -> e.getValue() != null);
    }

    @SuppressWarnings("unchecked")
    private static final UnmodifiableSortedMap<Object, Object> EMPTY = new UnmodifiableSortedMap<>(
            0, NO_ENTRIES, null);

    private final transient Comparator<K> comparator;
    private final transient Comparator<Entry<K, V>> entryComparator;

}
