package net.morimekta.util.collect;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableListCollector<E> implements Collector<E, UnmodifiableList.Builder<E>, UnmodifiableList<E>> {
    @Override
    public Supplier<UnmodifiableList.Builder<E>> supplier() {
        return UnmodifiableList::builder;
    }

    @Override
    public BiConsumer<UnmodifiableList.Builder<E>, E> accumulator() {
        return UnmodifiableList.Builder::add;
    }

    @Override
    public BinaryOperator<UnmodifiableList.Builder<E>> combiner() {
        return (b1, b2) -> b1.addAll(b2.build());
    }

    @Override
    public Function<UnmodifiableList.Builder<E>, UnmodifiableList<E>> finisher() {
        return UnmodifiableList.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf();
    }
}
