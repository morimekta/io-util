package net.morimekta.util.collect;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

class UnmodifiableSetCollector<E> implements Collector<E, UnmodifiableSet.Builder<E>, UnmodifiableSet<E>> {
    @Override
    public Supplier<UnmodifiableSet.Builder<E>> supplier() {
        return UnmodifiableSet::builder;
    }

    @Override
    public BiConsumer<UnmodifiableSet.Builder<E>, E> accumulator() {
        return UnmodifiableSet.Builder::add;
    }

    @Override
    public BinaryOperator<UnmodifiableSet.Builder<E>> combiner() {
        return (b1, b2) -> b1.addAll(b2.build());
    }

    @Override
    public Function<UnmodifiableSet.Builder<E>, UnmodifiableSet<E>> finisher() {
        return UnmodifiableSet.Builder::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return UnmodifiableSet.setOf();
    }
}
