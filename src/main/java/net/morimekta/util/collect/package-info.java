/**
 * This package contains a number of unmodifiable collection classes
 * largely inspired of the immutable collections in
 * <a href="https://github.com/google/guava">guava</a>. Note that there
 * are some important differences:
 *
 * <ul>
 *     <li>
 *         All classes are made as simple as possible, only one implementation
 *         per type (e.g. no special 1-entry set implementation).
 *     </li>
 *     <li>
 *         Builder classes for map and set accepts duplicate values / duplicate
 *         keys, and will handle them as a normal {@link java.util.Map} or
 *         {@link java.util.Set} would.
 *     </li>
 *     <li>
 *         The unmodifiable collections are <b>not</b> serializable, and if
 *         forcefully serialized will always end up empty, and as new instances.
 *         <i><b>PS: </b>The whole concept of serializable should be ditched in
 *         my opinion.</i>
 *     </li>
 *     <li>
 *         Note that since the classes were more or less written from scratch,
 *         I have taken the liberty of taking the general copyright of the
 *         implementations, though lots of kudos to the Guava team for making
 *         them in the first place, and more or less standardizing the interface
 *         for immutable collections. <i>Even Java uses these now...</i>
 *     </li>
 * </ul>
 *
 * Still there are a number of similarities, though these were not always explicitly
 * stated as features in Guava:
 *
 * <ul>
 *     <li>
 *         The list sets and maps still require all values to be non-null.
 *     </li>
 *     <li>
 *         The {@link net.morimekta.util.collect.UnmodifiableSet} and
 *         {@link net.morimekta.util.collect.UnmodifiableMap} classes will always
 *         preserve input order.
 *     </li>
 *     <li>
 *         Constructors will try to reuse the same instance and backing instances
 *         as much as possible, with the limitations of local knowledge.
 *     </li>
 * </ul>
 *
 * The classes implemented here have been made to be compact and fast, not to
 * be functional utilities. Still there are some utility methods added, one
 * to make all collections into lists, and one to make maps explicitly ordered.
 * See {@link net.morimekta.util.collect.UnmodifiableCollection#asList()} and
 * {@link net.morimekta.util.collect.UnmodifiableMapBase#orderedBy(java.util.Comparator)}.
 */
package net.morimekta.util.collect;