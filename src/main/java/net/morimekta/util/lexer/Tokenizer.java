package net.morimekta.util.lexer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

public interface Tokenizer<TT, T extends Token<TT>> {
    /**
     * Continue parsing content and return the next token to be found.
     *
     * @return The next token, or null if there is none.
     * @throws LexerException If parsing token failed.
     * @throws IOException If reading failed.
     */
    @Nullable
    T parseNextToken() throws LexerException, IOException;

    /**
     * Read all content until the given terminator string is encountered. The
     * terminator should <b>not</b> become part of the returned token. The
     * returned sequence may span more than one line.
     *
     * @param terminator The terminator string.
     * @param type The token type for the resulting token.
     * @param allowEof Set to true if EOF is allowed as a replacement for the terminator.
     * @return The char sequence from the current position until the encountered
     *         terminator or the end of the file. Or null if no chars before the terminator.
     * @throws LexerException On parse errors or validation failure.
     * @throws IOException If unable to parse token.
     */
    @Nullable
    T readUntil(@Nonnull CharSequence terminator,
                @Nonnull TT type,
                boolean allowEof) throws LexerException, IOException;

    /**
     * Get the current line number. Note that the lines should be 1-indexed, meaning
     * the first line (before the first newline) is line no 1. This should point to
     * the position where it will continue parsing, any previous tokens should be
     * represented by itself.
     *
     * @return The current line number.
     */
    int currentLineNo();

    /**
     * Get the current line position. This should point to the char position of the
     * current line, meaning it is 0-indexed. This should point to
     * the position where it will continue parsing, any previous tokens should be
     * represented by itself.
     *
     * @return The current line position.
     */
    int currentLinePos();

    /**
     * @return Content of the current line. The char-sequence must be effectively
     *         immutable.
     */
    @Nonnull
    CharSequence currentLine();
}
