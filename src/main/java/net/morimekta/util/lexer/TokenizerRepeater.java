package net.morimekta.util.lexer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * the tokenizer repeater is meant to repeat a set of tokens in the same
 * sequence.
 *
 * @param <TT> TokenType type.
 * @param <T> Token type.
 */
public class TokenizerRepeater<TT, T extends Token<TT>> implements Tokenizer<TT, T> {
    private final Queue<T> unread;
    private       T        lastToken;

    public TokenizerRepeater(List<T> tokens) {
        if (tokens.isEmpty()) {
            throw new IllegalArgumentException("Empty token list cannot be repeated");
        }

        unread = new ArrayDeque<>();
        unread.addAll(tokens);
    }

    @Nullable
    @Override
    public T parseNextToken() {
        if (unread.size() > 0) {
            lastToken = unread.poll();
            return lastToken;
        }
        return null;
    }

    @Nullable
    @Override
    public T readUntil(@Nonnull CharSequence terminator, @Nonnull TT type, boolean allowEof) throws IOException {
        if (unread.size() > 0) {
            if (!Objects.equals(unread.peek().type(), type)) {
                throw new LexerException("type mismatch: " + type + " != " + lastToken.type());
            }
            lastToken = requireNonNull(unread.poll());
            return lastToken;
        }
        if (allowEof) return null;
        throw new LexerException(currentLine(), currentLineNo(), currentLinePos(), 1,
                                 "End of stream reading until '" + terminator + "'");
    }

    @Override
    public int currentLineNo() {
        if (lastToken != null) {
            return lastToken.lineNo();
        }
        T next = requireNonNull(unread.peek());
        return next.lineNo();
    }

    @Override
    public int currentLinePos() {
        if (lastToken != null) {
            return lastToken.linePos() + lastToken.length();
        }
        T next = requireNonNull(unread.peek());
        return next.linePos();
    }

    @Nonnull
    @Override
    public CharSequence currentLine() {
        if (lastToken != null) {
            return lastToken.line();
        }
        T next = requireNonNull(unread.peek());
        return next.line();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
               unread.stream()
                     .map(TokenizerRepeater::quote)
                     .collect(Collectors.joining(", ")) + "}";
    }

    private static String quote(Token t) {
        return "'" + t + "'";
    }
}
