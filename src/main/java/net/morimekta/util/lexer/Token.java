package net.morimekta.util.lexer;

import net.morimekta.util.CharSlice;
import net.morimekta.util.Strings;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Arrays;
import java.util.Objects;

import static java.lang.Character.isHighSurrogate;
import static java.lang.Character.isLowSurrogate;

/**
 * A token parsed out of a tokenizer.
 *
 * @param <Type> The token type represented by the token.
 */
@Immutable
public class Token<Type> extends CharSlice {
    /**
     * Create a slice instance. The slice is only meant to be internal state
     * immutable, and not representing an immutable byte content.
     *
     * @param fb  The buffer to wrap.
     * @param off The start offset to wrap.
     * @param len The length to represent.
     * @param type The token type represented.
     * @param lineNo The current line number, starting at 1.
     * @param linePos The current line position, starting at 1.
     */
    public Token(char[] fb, int off, int len, @Nonnull Type type, int lineNo, int linePos) {
        super(fb, off, len);
        this.type = type;
        this.lineNo = lineNo;
        this.linePos = linePos;
    }

    /**
     * @return The token type.
     */
    @Nonnull
    public Type type() {
        return type;
    }

    /**
     * @return The line where the token is.
     */
    @Nonnull
    public CharSequence line() {
        if (line == null) {
            int start = off + 1 - linePos;
            int end   = off + len - 1;
            while (end < fb.length && fb[end] != '\n' && fb[end] != '\r' && fb[end] > 0) {
                ++end;
            }
            if (start < 0) {
                // pad start of line with spaces to make it the correct length.
                line = (start < -3
                        ? Strings.times("·", -3 - start) + "   "
                        : Strings.times(" ", -start)) +
                       new CharSlice(fb, 0, end);
            } else {
                line = new CharSlice(fb, start, end - start);
            }
        }
        return line;
    }

    /**
     * @return The the line number where this token is.
     */
    public int lineNo() {
        return lineNo;
    }

    /**
     * @return The the line position where the token starts.
     */
    public int linePos() {
        return linePos;
    }

    /**
     * Get the whole slice as a string.
     *
     * @param strict If it should validate string content strictly.
     * @return Slice decoded as UTF_8 string handling escaped characters.
     */
    public String decodeString(boolean strict) {
        // This decodes the string from UTF_8 bytes.
        CharSequence  tmp = substring(1, -1);
        final int     l   = tmp.length();
        StringBuilder out = new StringBuilder(l);

        char surrogate = 0;
        boolean esc = false;
        for (int i = 0; i < l; ++i) {
            char ch = tmp.charAt(i);
            if (surrogate > 0) {
                if (esc) {
                    if (ch == 'u') {
                        if (l < i + 5) {
                            if (strict) {
                                throw new IllegalArgumentException("Invalid escaped unicode char: '\\" +
                                                                   Strings.escape(tmp.subSequence(i, l)) +
                                                                   "'");
                            }
                            out.append("??");
                        } else {
                            String n = tmp.subSequence(i + 1, i + 5).toString();
                            try {
                                ch = (char) Integer.parseInt(n, 16);
                                if (isLowSurrogate(ch)) {
                                    out.append(surrogate);
                                    out.append(ch);
                                } else if (strict) {
                                    throw new IllegalArgumentException(String.format(
                                            "Unmatched high surrogate char: '\\u%04x'",
                                            (int) surrogate));
                                } else {
                                    out.append("??");
                                }
                            } catch (NumberFormatException e) {
                                if (strict) {
                                    throw new IllegalArgumentException("Invalid escaped unicode char: '\\u" +
                                                                       Strings.escape(n) +
                                                                       "'");
                                }
                                out.append("??");
                            }
                        }
                        i += 4;  // skipping 4 more characters.
                        surrogate = 0;
                        continue;
                    } else {
                        // mismatch
                        if (strict) {
                            throw new IllegalArgumentException(String.format(
                                    "Unmatched high surrogate char: '\\u%04x'",
                                    (int) surrogate));
                        }
                        out.append("?");
                        surrogate = 0;
                        // and fall down to 'normal' handling.
                    }
                } else if (ch == '\\') {
                    esc = true;
                    continue;
                } else if (isLowSurrogate(ch)) {
                    out.append(surrogate);
                    out.append(ch);
                    surrogate = 0;
                    continue;
                } else {
                    if (strict) {
                        throw new IllegalArgumentException(String.format(
                                "Unmatched high surrogate char: '\\u%04x'",
                                (int) surrogate));
                    }
                    out.append("?");
                    surrogate = 0;
                    // and fall down to 'normal' handling.
                }
            }
            if (esc) {
                esc = false;
                switch (ch) {
                    case 'b':
                        out.append('\b');
                        break;
                    case 'f':
                        out.append('\f');
                        break;
                    case 'n':
                        out.append('\n');
                        break;
                    case 'r':
                        out.append('\r');
                        break;
                    case 't':
                        out.append('\t');
                        break;
                    case '\"':
                    case '\'':
                    case '\\':
                        out.append(ch);
                        break;
                    case 'u':
                        if (l < i + 5) {
                            if (strict) {
                                throw new IllegalArgumentException("Invalid escaped unicode char: '\\" +
                                                                   Strings.escape(tmp.subSequence(i, l)) +
                                                                   "'");
                            }
                            out.append('?');
                        } else {
                            String n = tmp.subSequence(i + 1, i + 5).toString();
                            try {
                                char cp = (char) Integer.parseInt(n, 16);
                                if (isHighSurrogate(cp)) {
                                    surrogate = cp;
                                } else if (isLowSurrogate(cp)) {
                                    if (strict) {
                                        throw new IllegalArgumentException(String.format(
                                                "Unmatched low surrogate char: '\\u%04x'",
                                                (int) cp));
                                    }
                                    out.append("?");
                                } else {
                                    out.append(cp);
                                }
                            } catch (NumberFormatException e) {
                                if (strict) {
                                    throw new IllegalArgumentException("Invalid escaped unicode char: '\\u" +
                                                                       Strings.escape(n) +
                                                                       "'");
                                }
                                out.append('?');
                            }
                        }
                        i += 4;  // skipping 4 more characters.
                        break;
                    case '0':
                        if (l == i + 1 ||
                            (l > i + 1 && (tmp.charAt(i + 1) < '0' || tmp.charAt(i + 1) > '9'))) {
                            // allow single digit '\0' if the next char is not a digit.
                            out.append('\0');
                            break;
                        }
                        // Intentional fallthrough
                    case '1':
                        if (l < (i + 3)) {
                            if (strict) {
                                throw new IllegalArgumentException("Invalid escaped char: '\\" +
                                                                   Strings.escape(tmp.subSequence(i, l)) +
                                                                   "'");
                            }
                            out.append('?');
                        } else {
                            String n = tmp.subSequence(i, i + 3).toString();
                            try {
                                int cp = Integer.parseInt(n, 8);
                                out.append((char) cp);
                            } catch (NumberFormatException e) {
                                if (strict) {
                                    throw new IllegalArgumentException("Invalid escaped char: '\\" +
                                                                       Strings.escape(n) +
                                                                       "'");
                                }
                                out.append("?");
                            }
                        }
                        i += 2;  // skipping 2 more characters.
                        break;
                    default:
                        if (strict) {
                            throw new IllegalArgumentException("Invalid escaped char: '\\" +
                                                               Strings.escape(String.valueOf(ch)) +
                                                               "'");
                        }
                        out.append("?");
                        break;
                }
            } else if (ch == '\\') {
                esc = true;
            } else if (isHighSurrogate(ch)) {
                surrogate = ch;
            } else if (isLowSurrogate(ch)) {
                // unmatched low surrogate
                if (strict) {
                    throw new IllegalArgumentException(String.format(
                            "Unmatched low surrogate char: '\\u%04x'",
                            (int) ch));
                }
                out.append('?');
            } else {
                out.append(ch);
            }
        }
        if (surrogate > 0) {
            if (strict) {
                throw new IllegalArgumentException(String.format(
                        "Unmatched high surrogate char: '\\u%04x'",
                        (int) surrogate));
            }
            out.append("?");
        }
        return out.toString();
    }

    public boolean isSymbol(char symbol) {
        return len == 1 && fb[off] == symbol;
    }

    // --- Object ---

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null || !o.getClass().equals(getClass())) {
            return false;
        }
        Token other = (Token) o;

        return toString().equals(other.toString()) &&
               off == other.off &&
               len == other.len &&
               type == other.type &&
               lineNo == other.lineNo &&
               linePos == other.linePos;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClass(), Arrays.hashCode(fb), off, len, type, lineNo, linePos);
    }

    // --- PRIVATE ---
    protected final Type type;
    protected final int  lineNo;
    protected final int  linePos;

    @Nullable
    private transient CharSequence line;
}
