package net.morimekta.util.lexer;

import net.morimekta.util.Displayable;

import javax.annotation.Nonnull;
import java.io.UncheckedIOException;
import java.util.Objects;

/**
 * Exception representing problems parsing tokens or other problems with
 * returned tokens from the {@link Lexer}, or how a token interacts with
 * other in the whole structure. This exception is not meant to represent
 * problems that solely comes from the interaction between tokens in an
 * otherwise sound document, e.g. where the problem can not be pinned
 * to a single token causing the problem.
 */
public class UncheckedLexerException extends UncheckedIOException implements Displayable {
    /**
     * Make exception cause by secondary problems.
     *
     * @param cause The cause of the exception.
     */
    public UncheckedLexerException(@Nonnull LexerException cause) {
        super(cause.getMessage(), cause);
    }

    @Nonnull
    @Override
    public synchronized LexerException getCause() {
        return Objects.requireNonNull((LexerException) super.getCause());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + getMessage() + "}";
    }

    @Nonnull
    public String displayString() {
        return getCause().displayString();
    }
}
