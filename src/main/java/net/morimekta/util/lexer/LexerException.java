package net.morimekta.util.lexer;

import net.morimekta.util.Displayable;
import net.morimekta.util.Strings;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

import static net.morimekta.util.Strings.isConsolePrintable;

/**
 * Exception representing problems parsing tokens or other problems with
 * returned tokens from the {@link Lexer}, or how a token interacts with
 * other in the whole structure. This exception is not meant to represent
 * problems that solely comes from the interaction between tokens in an
 * otherwise sound document, e.g. where the problem can not be pinned
 * to a single token causing the problem.
 */
public class LexerException extends IOException implements Displayable {
    private CharSequence line;
    private int          lineNo = -1;
    private int          linePos = -1;
    private int          length = 0;
    private Token        token;

    /**
     * Make exception cause by secondary problems.
     *
     * @param message Exception message.
     */
    protected LexerException(@Nonnull String message) {
        super(message);
    }

    /**
     * Make exception cause by secondary problems.
     *
     * @param cause The cause of the exception.
     * @param message Exception message.
     */
    protected LexerException(@Nonnull Throwable cause, @Nonnull String message) {
        super(message, cause);
        if (cause instanceof LexerException) {
            LexerException e = (LexerException) cause;
            if (e.token != null) {
                this.token = e.token;
            } else {
                this.lineNo = e.getLineNo();
                this.linePos = e.getLinePos();
                this.length = e.getLength();
                this.line = e.getLine();
            }
        }
    }

    /**
     * Make exception representing problems with a parsed token.
     *
     * @param token The problematic token.
     * @param message The exception message.
     */
    public LexerException(@Nonnull Token token, @Nonnull String message) {
        super(message);
        this.token = token;
    }

    /**
     * Make exception representing a tokenizing problem at a specific position
     * in the input.
     *
     * @param line The line the problem is located.
     * @param lineNo The line number where the problem is (1-N).
     * @param linePos The position in the line where the problem is (0-N).
     * @param length Length of the error.
     * @param message The exception message.
     */
    public LexerException(@Nonnull CharSequence line, int lineNo, int linePos, int length, @Nonnull String message) {
        super(message);
        this.line = line;
        this.length = length;
        this.lineNo = lineNo;
        this.linePos = linePos;
    }

    @Override
    public LexerException initCause(Throwable cause) {
        super.initCause(cause);
        return this;
    }

    /**
     * @return The line string where the problem occurred.
     */
    @Nullable
    public CharSequence getLine() {
        if (token != null) {
            return token.line();
        }
        return line;
    }

    /**
     * @return The line number where the problem occurred.
     */
    public int getLineNo() {
        if (token != null) {
            return token.lineNo();
        }
        return lineNo;
    }

    /**
     * @return The line position where the problem occurred.
     */
    public int getLinePos() {
        if (token != null) {
            return token.linePos();
        }
        return linePos;
    }

    /**
     * @return Length of match where fault lies.
     */
    public int getLength() {
        if (token != null) {
            return token.length();
        }
        return length;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "\n" + displayString();
    }

    /**
     * Replace non-printable chars in a string with something else. The
     * replacement is static and only meant as a place-holder. It is advised
     * to use a non-standard char as the replacement, as otherwise it will
     * not be distinguishable from the standard "printable".
     *
     * @param str The string to escape.
     * @param replacement Char to replace non-printable with.
     * @return The escaped char string.
     */
    public static String replaceNonPrintable(CharSequence str, char replacement) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            if (isConsolePrintable(c)) {
                builder.append(c);
            } else {
                builder.append(replacement);
            }
        }
        return builder.toString();
    }

    protected String getError() {
        return "Error";
    }

    @Nonnull
    @Override
    public String displayString() {
        if (token != null) {
            if (token.length() > 1) {
                return String.format(
                        "%s on line %d row %d-%d: %s%n" +
                        "%s%n" +
                        "%s%s",
                        getError(),
                        token.lineNo(),
                        token.linePos(),
                        token.linePos() + token.length() - 1,
                        getLocalizedMessage(),
                        replaceNonPrintable(token.line(), '·'),
                        Strings.times("-", token.linePos() - 1),
                        Strings.times("^", token.length()));
            }
            return String.format(
                    "%s on line %d row %d: %s%n" +
                    "%s%n" +
                    "%s^",
                    getError(),
                    token.lineNo(),
                    token.linePos(),
                    getLocalizedMessage(),
                    replaceNonPrintable(token.line(), '·'),
                    Strings.times("-", token.linePos() - 1));
        } else if (line != null) {
            if (length > 1) {
                return String.format(
                        "%s on line %d row %d-%d: %s%n" +
                        "%s%n" +
                        "%s%s",
                        getError(),
                        lineNo,
                        linePos,
                        linePos + length - 1,
                        getLocalizedMessage(),
                        replaceNonPrintable(line, '·'),
                        Strings.times("-", linePos - 1),
                        Strings.times("^", length));
            }
            return String.format(
                    "%s on line %d row %d: %s%n" +
                    "%s%n" +
                    "%s^",
                    getError(),
                    lineNo,
                    linePos,
                    getLocalizedMessage(),
                    replaceNonPrintable(line, '·'),
                    Strings.times("-", linePos - 1));
        } else {
            return String.format("%s: %s", getError(), getLocalizedMessage());
        }
    }
}
