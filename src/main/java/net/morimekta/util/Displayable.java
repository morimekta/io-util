package net.morimekta.util;

import javax.annotation.Nonnull;

/**
 * Interface for making objects displayable. This differs from
 * {@link Stringable} by being meant to generate human understandable
 * strings, not just 'string' values representing the instances.
 */
public interface Displayable {
    /**
     * @return A displayable string value for the object. This is meant to
     *         be printed e.g. to console output and interpreted by a human.
     */
    @Nonnull
    String displayString();
}
