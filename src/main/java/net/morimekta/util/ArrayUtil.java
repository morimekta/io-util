package net.morimekta.util;

import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

/**
 * Simple utility for handling primitive arrays.
 */
public class ArrayUtil {
    /**
     * @param booleans Array of booleans.
     * @return List of booleans.
     */
    public static List<Boolean> listPrimitives(@Nonnull boolean[] booleans) {
        if (booleans.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Boolean> builder = UnmodifiableList.builder(booleans.length);
        for (boolean b : booleans) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param booleans Collection of booleans.
     * @return Primitive array for same booleans.
     */
    public static boolean[] booleanPrimitives(@Nonnull Collection<Boolean> booleans) {
        if (booleans.isEmpty()) return NO_BOOLEANS;
        boolean[] out = new boolean[booleans.size()];
        int i = 0;
        for (Boolean bool : booleans) {
            out[i++] = bool;
        }
        return out;
    }

    /**
     * @param booleans Array of booleans.
     * @return Primitive array for same booleans.
     */
    public static boolean[] booleanPrimitives(Boolean... booleans) {
        if (booleans.length == 0) return NO_BOOLEANS;
        boolean[] out = new boolean[booleans.length];
        for (int i = 0; i < booleans.length; ++i) {
            out[i] = booleans[i];
        }
        return out;
    }

    /**
     * @param bytes Array of bytes.
     * @return List of bytes.
     */
    public static List<Byte> listPrimitives(@Nonnull byte[] bytes) {
        if (bytes.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Byte> builder = UnmodifiableList.builder(bytes.length);
        for (byte b : bytes) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param bytes Collection of bytes.
     * @return Primitive array for same bytes.
     */
    public static byte[] bytePrimitives(@Nonnull Collection<Byte> bytes) {
        if (bytes.isEmpty()) return NO_BYTES;
        byte[] out = new byte[bytes.size()];
        int i = 0;
        for (Byte b : bytes) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param bytes Array of bytes.
     * @return Primitive array for same bytes.
     */
    public static byte[] bytePrimitives(Byte... bytes) {
        if (bytes.length == 0) return NO_BYTES;
        byte[] out = new byte[bytes.length];
        for (int i = 0; i < bytes.length; ++i) {
            out[i] = bytes[i];
        }
        return out;
    }

    /**
     * @param shorts Array of shorts.
     * @return List of shorts.
     */
    public static List<Short> listPrimitives(@Nonnull short[] shorts) {
        if (shorts.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Short> builder = UnmodifiableList.builder(shorts.length);
        for (short b : shorts) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param shorts Collection of shorts.
     * @return Primitive array of same shorts.
     */
    public static short[] shortPrimitives(@Nonnull Collection<Short> shorts) {
        if (shorts.isEmpty()) return NO_SHORTS;
        short[] out = new short[shorts.size()];
        int i = 0;
        for (Short b : shorts) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param shorts Array of shorts.
     * @return Primitive array of same shorts.
     */
    public static short[] shortPrimitives(Short... shorts) {
        if (shorts.length == 0) return NO_SHORTS;
        short[] out = new short[shorts.length];
        for (int i = 0; i < shorts.length; ++i) {
            out[i] = shorts[i];
        }
        return out;
    }

    /**
     * @param ints Array of ints.
     * @return List of ints.
     */
    public static List<Integer> listPrimitives(@Nonnull int[] ints) {
        if (ints.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Integer> builder = UnmodifiableList.builder(ints.length);
        for (int b : ints) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param ints Collection of integers.
     * @return Primitive array of same ints.
     */
    public static int[] intPrimitives(@Nonnull Collection<Integer> ints) {
        if (ints.isEmpty()) return NO_INTS;
        int[] out = new int[ints.size()];
        int i = 0;
        for (Integer b : ints) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param ints Array of integers.
     * @return Primitive array of same ints.
     */
    public static int[] intPrimitives(Integer... ints) {
        if (ints.length == 0) return NO_INTS;
        int[] out = new int[ints.length];
        for (int i = 0; i < ints.length; ++i) {
            out[i] = ints[i];
        }
        return out;
    }

    /**
     * @param longs Array of longs.
     * @return List of longs.
     */
    public static List<Long> listPrimitives(@Nonnull long[] longs) {
        if (longs.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Long> builder = UnmodifiableList.builder(longs.length);
        for (long b : longs) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param longs Collection of longs.
     * @return Primitive array of same longs.
     */
    public static long[] longPrimitives(@Nonnull Collection<Long> longs) {
        if (longs.isEmpty()) return NO_LONGS;
        long[] out = new long[longs.size()];
        int i = 0;
        for (Long b : longs) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param longs Array of longs.
     * @return Primitive array of same longs.
     */
    public static long[] longPrimitives(Long... longs) {
        if (longs.length == 0) return NO_LONGS;
        long[] out = new long[longs.length];
        for (int i = 0; i < longs.length; ++i) {
            out[i] = longs[i];
        }
        return out;
    }

    /**
     * @param floats Array of floats.
     * @return List of floats.
     */
    public static List<Float> listPrimitives(@Nonnull float[] floats) {
        if (floats.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Float> builder = UnmodifiableList.builder(floats.length);
        for (float b : floats) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param floats Collection of floats.
     * @return Primitive array of same floats.
     */
    public static float[] floatPrimitives(@Nonnull Collection<Float> floats) {
        if (floats.isEmpty()) return NO_FLOATS;
        float[] out = new float[floats.size()];
        int i = 0;
        for (Float b : floats) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param floats Array of floats.
     * @return Primitive array of same floats.
     */
    public static float[] floatPrimitives(Float... floats) {
        if (floats.length == 0) return NO_FLOATS;
        float[] out = new float[floats.length];
        for (int i = 0; i < floats.length; ++i) {
            out[i] = floats[i];
        }
        return out;
    }

    /**
     * @param doubles Array of doubles.
     * @return List of doubles.
     */
    public static List<Double> listPrimitives(@Nonnull double[] doubles) {
        if (doubles.length == 0) return UnmodifiableList.listOf();
        UnmodifiableList.Builder<Double> builder = UnmodifiableList.builder(doubles.length);
        for (double b : doubles) {
            builder.add(b);
        }
        return builder.build();
    }

    /**
     * @param doubles Collection of doubles.
     * @return Primitive array of same doubles.
     */
    public static double[] doublePrimitives(@Nonnull Collection<Double> doubles) {
        if (doubles.isEmpty()) return NO_DOUBLES;
        double[] out = new double[doubles.size()];
        int i = 0;
        for (Double b : doubles) {
            out[i++] = b;
        }
        return out;
    }

    /**
     * @param doubles Array of doubles.
     * @return Primitive array of same doubles.
     */
    public static double[] doublePrimitives(Double... doubles) {
        if (doubles.length == 0) return NO_DOUBLES;
        double[] out = new double[doubles.length];
        for (int i = 0; i < doubles.length; ++i) {
            out[i] = doubles[i];
        }
        return out;
    }

    private ArrayUtil() {}

    private static final boolean[] NO_BOOLEANS = new boolean[0];
    private static final byte[] NO_BYTES = new byte[0];
    private static final short[] NO_SHORTS = new short[0];
    private static final int[] NO_INTS = new int[0];
    private static final long[] NO_LONGS = new long[0];
    private static final float[] NO_FLOATS = new float[0];
    private static final double[] NO_DOUBLES = new double[0];
}
